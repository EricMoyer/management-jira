#!/usr/bin/env python3.9

import argparse
import collections
import getpass
from typing import Optional

import lib
import pandas
import random


def issues(
    source_server,
    open_or_closed,
    type_,
    project,
    max_results,
    additional_criteria=None,
):
    """
    Retrieve issues from server

    open_or_closed: 'open' if issues should be unclosed or 'closed' if not
    type_: string containing the issue type e.g. "bug"
    project: the jira project queue string e.g. "RSNP"
    max_results: False for all or an integer giving the maximum results to
        return
    additional_criteria: additional criteria in JQL to AND to the list.
        Nothing is ANDed if None
    """
    not_ = "" if open_or_closed == "closed" else "not "
    # noinspection SpellCheckingInspection
    return source_server.search_issues(
        "status {}in (Closed, Resolved) AND type = {} AND project = {} "
        "{}ORDER BY key DESC".format(
            not_,
            type_,
            project,
            ""
            if additional_criteria is None
            else "AND " + additional_criteria + " ",
        ),
        fields="aggregatetimespent,aggregatetimeoriginalestimate",
        maxResults=max_results,
    )


parser = argparse.ArgumentParser(
    description="Statistics about " "average logged time for completed tickets"
)
parser.add_argument(
    "project", help="The project jira queue to search for tickets"
)
parser.add_argument(
    "type",
    help="The type of ticket whose average time statistics "
    "are computed e.g. bug",
)
parser.add_argument(
    "--username",
    help="Username to for basic authentication login. "
    "Defaults to shell username.",
    default=getpass.getuser(),
)
parser.add_argument(
    "--max_results",
    help="Maximum results to return ... defaults to all",
    default=False,
    type=lib.positive_int,
)

parser.add_argument(
    "--num_estimate_bins",
    help="When binning original estimates use this " "many bins",
    type=lib.positive_int,
    default=8,
)
parser.add_argument(
    "--min_estimates_per_bin",
    help="If using num_estimate_bins bins would "
    "make the number of estimates per bin lower than this reduce the number "
    "of bins until this holds or there is only one bin",
    type=lib.positive_int,
    default=8,
)
parser.add_argument(
    "--num_samples",
    help="Number of samples to take when estimating "
    "distributions of future completion times",
    type=lib.positive_int,
    default=1000,
)
parser.add_argument(
    "--credible_interval",
    help="The size of the credible interval to "
    "calculate for future values. A 95%% interval would use 0.95",
    type=lib.probability_float,
    default=0.95,
)
parser.add_argument(
    "--open_issues_jql",
    help="Additional JQL to AND into the query for "
    "the open issues whose completion estimate will be calculated",
    default=None,
)
parser.add_argument(
    "--num_new_issues_to_estimate",
    help="Number of new randomly generated "
    "open issues to add to the real number of open issues in creating "
    "the estimates",
    default=0,
    type=lib.positive_int,
)

args = parser.parse_args()

server = lib.ncbi_jira(args.username)

closed_issues = issues(
    server, "closed", args.type, args.project, args.max_results
)
open_issues = issues(
    server,
    "open",
    args.type,
    args.project,
    args.max_results,
    additional_criteria=args.open_issues_jql,
)

closed_issues_with_time = tuple(
    i for i in closed_issues if i.fields.aggregatetimespent
)

closed_issues_with_estimate_and_time = tuple(
    i
    for i in closed_issues_with_time
    if i.fields.aggregatetimeoriginalestimate
    and i.fields.aggregatetimeoriginalestimate > 0
)

num_estimate_bins = args.num_estimate_bins
num_estimates = len(closed_issues_with_estimate_and_time)
if num_estimates // num_estimate_bins < args.min_estimates_per_bin:
    num_estimate_bins = max(
        1,
        len(closed_issues_with_estimate_and_time) // args.min_estimates_per_bin,
    )
categorizer = lib.QuantileCategorizer(
    closed_issues_with_estimate_and_time,
    num_estimate_bins,
    lib.aggregated_original,
)

# issues_in[i] has all issues with estimates whose original estimate
#     was in category i.
#
# issues_in[None] has all issues whether they had a category or not
issues_in: dict[Optional[str], list] = collections.defaultdict(list)
issues_in[None] = list(closed_issues_with_time)
for i in closed_issues_with_time:
    cat = categorizer.category(i)
    if cat is not None:
        issues_in[cat].append(i)

# Sample num_sample times for each issue
issue_times = []
issues_to_estimate = list(open_issues)
if args.num_new_issues_to_estimate > 0:
    issues_to_estimate.extend(
        random.choices(
            closed_issues + open_issues, k=args.num_new_issues_to_estimate
        )
    )
for i in issues_to_estimate:
    cat = issues_in[categorizer.category(i)]
    issue_times.append(
        map(lib.aggregated_actual, random.choices(cat, k=args.num_samples))
    )

# Total times[i] is a sample from the distribution of the sums of the
# times taken by all the open issues ... i.e. a sample from the distribution
# of the time required to complete everything
total_times = pandas.Series(map(sum, zip(*issue_times)))


hours = pandas.Series(map(lib.aggregated_actual, closed_issues_with_time))
print(
    "{} resolved issues ({} with time). Mean hours: {:.2f}, quartiles: "
    "{:.2f},{:.2f},{:.2f}".format(
        len(closed_issues),
        len(closed_issues_with_time),
        hours.mean(),
        hours.quantile(0.25),
        hours.median(),
        hours.quantile(0.75),
    )
)

alpha = 1 - args.credible_interval
print(
    "There are {} open issues meeting the criteria. A simple model from the "
    "resolved issues yields a {:.0f}% credible interval of {:.2f}..{:.2f} "
    "hours to complete all the open tasks plus {} tasks selected randomly "
    "from the closed issues and the open issues meeting the criteria "
    "(median of {:.2f})".format(
        len(open_issues),
        args.credible_interval * 100,
        total_times.quantile(alpha / 2),
        total_times.quantile(1 - alpha / 2),
        args.num_new_issues_to_estimate,
        total_times.median(),
    )
)
