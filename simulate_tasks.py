#!/usr/bin/env python3.9
"""
Edit this script to calculate the total time for a collection of tasks -
including a number of unestimated tasks.

First, edit the distributions to match the historical times taken by tasks
with a given estimate.

Next, edit num_samples

Thirdly, edit the call to sample_time_and_unestimated to select how many tasks
to estimate.

Finally, run the script
"""

from statistics import mean, stdev
from lognormal import LogNormalAlternate
from itertools import chain, repeat
import random


# Size of any task in calendar hours
task = LogNormalAlternate(18.7495711775044, 23.6944636723585)

# Medium task
med = LogNormalAlternate(17.5565893719807, 13.4734715540916)

# Large task
large = LogNormalAlternate(38.9150483870968, 33.6034377956884)


def sample_time(num_unestimated: int, num_med: int, num_large: int) -> float:
    """
    Return a sample from the distribution of the time required to do
    the given number of unestimated, medium, and large tasks
    """
    return sum(
        map(
            lambda d: d.sample(),
            chain.from_iterable(
                [
                    repeat(dist, num)
                    for dist, num in (
                        (task, num_unestimated),
                        (med, num_med),
                        (large, num_large),
                    )
                ]
            ),
        )
    )


def sample_time_and_unestimated(
    min_unestimated: int, max_unestimated: int, num_med: int, num_large: int
) -> float:
    """
    Sample the number of unestimated tasks uniformly between min_unestimated
    and max_unestimated (inclusive) then call sample_time
    :param min_unestimated: The minimum number of unestimated tasks
    :param max_unestimated: The maximum number of unestimated tasks
    :param num_med: The number of medium tasks
    :param num_large: The number of large tasks
    :return: The sampled total time
    """
    return sample_time(
        random.randint(min_unestimated, max_unestimated), num_med, num_large
    )


num_samples = 200000
samples = [
    sample_time_and_unestimated(27, 42, 1, 4) for _ in range(num_samples)
]
print(
    f"Mean time: {mean(samples)} person-calendar hours, "
    f"Std: {stdev(samples)}"
)
