#!/usr/bin/env python3.9
import sys
from argparse import ArgumentParser
from pathlib import Path


def main() -> int:
    # Since this is a simple command line script for expert use it does
    # not handle exceptions or use logging.
    parser = ArgumentParser(
        description="Shift work log data directories to previous months."
        "At the end, it will create a new (empty) month-0 directory."
    )
    parser.add_argument(
        "--root-work-log-dir",
        type=str,
        default="work_log_data",
        help="Directory containing work log data directories. Directories "
        "should be named month-0 (for the most recent month), month-1 "
        "(for the previous month), etc.",
    )
    parser.add_argument(
        "--force",
        action="store_true",
        help="Force the shift even if the month-0 directory is empty.",
    )
    args = parser.parse_args()
    root = Path(args.root_work_log_dir)
    if root.is_file():
        print(f"{root} is a file, not a directory", file=sys.stderr)
        return 1
    root.mkdir(exist_ok=True)
    month_0 = root / "month-0"
    if month_0.is_dir() and not any(month_0.iterdir()) and not args.force:
        print(
            f"Not shifting because {month_0} is empty. "
            f"Use --force to shift anyway. "
            "This will create a new (empty) month-0 directory.",
            file=sys.stderr,
        )
        return 2
    # [0-9]* ensures we don't match month--1 or month-foo - it will match
    # non-integers that start with a digit, but we'll filter those out later
    month_num_strs = (str(i).split("-")[1] for i in root.glob("month-[0-9]*"))
    month_nums = sorted(
        (int(i) for i in month_num_strs if i.isdigit()), reverse=True
    )
    for i in month_nums:
        (root / f"month-{i}").rename(root / f"month-{i+1}")
    (root / "month-0").mkdir(exist_ok=True)
    return 0


if __name__ == "__main__":
    sys.exit(main())
