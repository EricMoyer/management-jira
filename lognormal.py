from math import log, sqrt
from dataclasses import dataclass
from typing import Union, Optional
import random

from numpy.random import Generator


@dataclass
class LogNormalStandard:
    """A log-normal distribution parameterized by the mean and
    standard deviation of its underlying Gaussian"""

    mu: float
    sigma: float

    def sample(self) -> float:
        """Return a sample from this distribution"""
        return random.lognormvariate(self.mu, self.sigma)


@dataclass
class LogNormalAlternate:
    """A log-normal distribution parameterized by its mean and
    standard deviation"""

    mean: float
    stddev: float

    def sample(self, rng: Optional[Generator] = None) -> float:
        if self.mean <= 0 or self.stddev == 0:
            raise ValueError(
                f"Log-normal distributions must have a positive mean and "
                f"standard deviation: mean={self.mean} std={self.stddev}"
            )
        v = self.stddev * self.stddev
        m2 = self.mean * self.mean
        mu = log(m2 / sqrt(m2 + v))
        sigma = sqrt(abs(2 * log(sqrt(m2 + v) / self.mean)))
        if rng:
            return rng.lognormal(mu, sigma)
        return random.lognormvariate(mu, sigma)


LogNormal = Union[LogNormalAlternate, LogNormalStandard]
