# How to generate a monthly status report

1. Open a terminal at `/export/home/moyered/ManagementJira/` and activate the
   virtual environment (you should have already installed all the requirements
   and have a file for your OpenAI product key and one for the Jira personal
   access token.)
   ```bash
   cd /export/home/moyered/ManagementJira/
   . ~/.virtualenvs/ManagementJira/bin/activate
   ```
2. Run the `make_msr.py` script. (The below invocation has the product key and
   personal access token files in the same directory as the script.) Note that
   it uses the default date ranges. There are command line arguments if you're
   in an unusual situation requiring a different date range.
   ```bash
   python make_msr.py moyered "Eric Moyer" open_ai_product_key.txt \
      jira_personal_access_token.txt --output-file msr.md \
      --prompt-file prompt_used.txt
   ```
   and
   ```powershell
    python make_msr.py moyered "Eric Moyer" open_ai_product_key.txt `
        jira_personal_access_token.txt --output-file msr.md `
        --prompt-file prompt_used.txt
   ```
3. Open the output file (`msr.md`) in PyCharm and make any necessary edits.
4. Copy the rendered view of the output file and paste it into this month's
   status report on OneDrive. (You may need to use https://stackedit.io/app)
   The web version of MS Word seems to transfer the formatting more consistently
   than the desktop version.
5. Attach the status report to the email and send it to the appropriate
   recipients.
