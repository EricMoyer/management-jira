#!/usr/bin/env python3.9

import argparse
import sys
from datetime import datetime, date, timedelta


def parse_args():
    parser = argparse.ArgumentParser(
        description="Generate a prompt to summarize monthly activities."
    )
    parser.add_argument(
        "--current_month_num", type=int, help="1 for Jan, 2 for Feb, etc."
    )
    parser.add_argument(
        "--current_year", type=int, help="The current year, e.g. 2021."
    )
    parser.add_argument(
        "month_minus_2_work_log_markdown",
        help="The markdown file with summarized work "
        "records for two months back.",
    )
    parser.add_argument(
        "month_minus_2_summary",
        help="Two months back's summary in Markdown as a bulleted list.",
    )
    parser.add_argument(
        "month_minus_1_work_log_markdown",
        help="The markdown file with summarized work "
        "records for the previous month.",
    )
    parser.add_argument(
        "month_minus_1_summary",
        help="Previous month's summary in Markdown as a bulleted list.",
    )
    parser.add_argument(
        "work_log_markdown",
        help="Markdown file with individually summarized work records",
    )
    return parser.parse_args()


def one_month_ago(cur_date: date) -> date:
    """Return the date one month before the current date."""
    one_day = timedelta(days=1)
    prev_date = cur_date - one_day
    while prev_date.month == cur_date.month:
        prev_date -= one_day
    while prev_date.day != cur_date.day:
        prev_date -= one_day
    return prev_date


def main() -> int:
    args = parse_args()

    # Calculate previous month and year
    real_date = datetime.now().date()
    # Start at previous month if we're writing this report in the first 21 days
    # of the month because that's too early to write a report for the current
    # month - so assume the user is just late in writing their report.
    start_date = one_month_ago(real_date) if real_date.day <= 21 else real_date
    cur_month = date(
        args.current_year if args.current_year else start_date.year,
        args.current_month_num if args.current_month_num else start_date.month,
        start_date.day,
    )
    month_minus_1 = one_month_ago(cur_month)
    month_minus_2 = one_month_ago(month_minus_1)

    # Read files
    month_minus_2_log_contents = slurp(args.month_minus_2_work_log_markdown)
    month_minus_2_summary_contents = slurp(args.month_minus_2_summary)
    month_minus_1_log_contents = slurp(args.month_minus_1_work_log_markdown)
    month_minus_1_summary_contents = slurp(args.month_minus_1_summary)
    log_contents = slurp(args.work_log_markdown)

    # Output content
    print(
        f"""I'd like you to help me with a monthly work summary list.

    I have summarized my activities summarized for each Jira ticket
    {log_contents}

    Below is my summary for the previous month. Please summarize the activities \
    above in the same type of multi-level bulleted list and level of detail I \
    used for the previous month using the imperative mood (that is, rather \
    than writing "Fixed the bug", write "Fix the bug".) Do not mention \
    Jira ticket numbers or give a title or any content except the bulleted \
    list. This is a high-level summary. Update the parts under \
    "DbGaP API / Integrate FHIR Research Data Finder with dbGaP API"

    {month_minus_1_summary_contents}
    """
    )
    return 0


def slurp(filename: str) -> str:
    with open(filename, "r") as issues:
        issue_csv_file_contents = issues.read()
    return issue_csv_file_contents


if __name__ == "__main__":
    sys.exit(main())
