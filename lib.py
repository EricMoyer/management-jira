# Library routines for operations needed by managers at NCBI
# (particularly MGV)
#
# Will break up into real modules once I have some clear idea what the
# structure is.


from typing import Optional
import argparse
import getpass
import jira
import math
import os
import pandas


def positive_int(value):
    """
    For use with argparse.add_argument type as a parameter to the type
    argument: allows only positive integers
    """
    try:
        i_value = int(value)
    except Exception as e:
        raise argparse.ArgumentTypeError(
            "{} is not an integer".format(value)
        ) from e

    if i_value <= 0:
        raise argparse.ArgumentTypeError("{} is not positive".format(value))

    return i_value


def probability_float(value):
    """
    For use with argparse.add_argument type as a parameter to the type
    argument: allows only real numbers between 0 and 1
    """
    try:
        fvalue = float(value)
    except Exception as e:
        raise argparse.ArgumentTypeError(
            "{} is not a float".format(value)
        ) from e

    if fvalue < 0 or fvalue > 1:
        raise argparse.ArgumentTypeError(
            "{} is not between 0 and 1".format(value)
        )

    return fvalue


def aggregated_original(issue):
    """
    Return sum of the times estimated for this issue and all of its subtasks

    Time in hours
    """
    v = issue.fields.aggregatetimeoriginalestimate
    return v if v is None else v / 3600


def aggregated_actual(issue):
    """
    Return sum of the times taken by this issue and all of its subtasks

    Time in hours
    """
    v = issue.fields.aggregatetimespent
    return v if v is None else v / 3600


def ncbi_jira(username: str, password_varname: Optional[str] = None):
    """Prompt for password and connect to ncbi jira for the given user

    username - the name of the user to log in as

    password_varname - if present, the environment variable from which
      to fetch the password. If absent, prompts for the password on
      from the terminal.
    """

    # Make sure this is HTTPS ... otherwise password might be revealed
    jira_url = "https://jira.ncbi.nlm.nih.gov"

    # Prompt for the password rather than passing it on the command
    # line because otherwise the password could be left in shell
    # commandline history or in terminal history ... relatively easily
    # obtainable by a hacker. The environment variable also provides a way
    # of avoiding having the password appear in history.
    try:
        password = os.environ[password_varname]  # type: ignore
    except (TypeError, KeyError):
        password = getpass.getpass(prompt="Enter your jira password: ")

    return jira.JIRA(
        options={"server": jira_url}, basic_auth=(username, password)
    )


class QuantileCategorizer:
    def __init__(self, objects, num_bins, value_function):
        """
        Calculate num_bins quantiles of applying value_function to objects

        value_function will be stored and must return a number that
        is comparable to math.inf or None ... None values will not be
        used to calculate quantiles
        """
        self._value_function = value_function

        try:
            bins = list(
                pandas.qcut(
                    tuple(
                        v
                        for v in map(self._value_function, objects)
                        if v is not None
                    ),
                    num_bins,
                    duplicates="drop",
                    retbins=True,
                )[1]
            )
            # Make sure no values are out-of-bounds by extending first
            # and last bins to have their outer edges at infinity
            bins[0] = -math.inf
            bins[-1] = math.inf
        except IndexError:
            # If there were so many duplicates that too many bin edges
            # ended up being dropped and the bin list became invalid
            # make one bin with all numbers
            bins = [-math.inf, math.inf]

        self._bins = bins

    def category(self, obj):
        """
        Return the (non-negative integer) quantile into which obj falls

        obj must be the same type as those used to initialize this categorizer

        if value_function(obj) is None, returns None
        """
        v = self._value_function(obj)
        return (
            v
            if v is None
            else pandas.cut(
                [self._value_function(obj)], self._bins, labels=False
            )[0]
        )
