#!/usr/bin/env python3.11
import logging
import sys
from datetime import date

from msr_jira import download_issues
from make_msr_parse_args import parse_args
from query_gpt4 import query_gpt4
from worklog_issue import Issue

log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler(sys.stderr))


def first_worklog_date(issue: Issue) -> date | None:
    """Return the earliest work log date from the issues."""
    return min(wl.date for wl in issue.work_logs)


def prompt_for(
    issues: dict[str, Issue], unretrieved_issues: set[str], name, key
) -> str:
    """Prompt requesting the LLM to summarize the work done this month.

    :param issues: The issues worked on or related to the work logs
    :param unretrieved_issues: The issues that could not be retrieved that were
        referenced in the work logs
    :param name: The name of the person whose work is being summarized
    :param key: The JIRA key of the person whose work is being summarized
    """
    main_issues = [i for i in issues.values() if len(i.work_logs) > 0]
    main_issues.sort(key=first_worklog_date)
    supplementary_issues = [i for i in issues.values() if len(i.work_logs) == 0]
    data = {
        "main_issues": main_issues,
        "supplementary_issues": supplementary_issues,
        "unretrieved_issues": unretrieved_issues,
    }
    return f"""
    Summarize the work done this month from the following work logs
    and issues. The summary is for a monthly status report for the user
    {name} ({key}). The summary should be at a high level. The intended 
    audience does not know about issue numbers or about ongoing work. Do not 
    include issue numbers like FHIR-1234 or SYS-987654. Since
    {name} will be submitting this report, the summary does not need to
    mention {name} by name. The summary should be a bulleted list and be in the 
    present tense.
    
    The following items are boilerplate for general repeating responsibilities:
    -	Investigate potential improvements and new technology
    -	Report for upper management decision-making
    -	Improve and maintain our on-boarding documentation
    -	Help debug and create post-mortem reports when our services are down
    -	Categorize our task backlog for the Product Owner
    -	Maintain a portfolio of products falling under the purview of the FHIR
        team lead
    -	Summarize team activities for stakeholders and get updated plans from
        them
    -	Attend team meetings and seminars
    -	Make final calls on design decisions
    -	Supervise teammates efforts
    -	Decide on what tasks get done and their priority
    -	Consult on technical details of teammates’ tasks
    -	Ensure team finishes mandatory training on time
    -	Act as liaison between developers and the rest of the organization

    Add the boilerplate at the end and include specific items for activities 
    that are not covered by the boilerplate.

    The summary should be in markdown.

    Here are the issues worked on this month along with their work logs and
    some supplementary issues referenced in the work logs or the main issues.

    {data}
    """


def main() -> int:
    args = parse_args()

    if args.dont_send:
        args.debug = True

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
        log.debug("Debug logging enabled")

    with open(args.open_api_token_file, "r") as file:
        open_api_token = file.read().strip()

    with open(args.jira_personal_access_token_file, "r") as file:
        jira_token = file.read().strip()

    jql = (
        f"worklogAuthor = currentUser() and "
        f"worklogDate >= {args.first_included_date} and "
        f"worklogDate < {args.first_excluded_date}"
    )

    issues: dict[str, Issue] = download_issues(
        args.jira_server_base, jira_token, jql
    )

    ignore_keys = args.ignore_keys

    related_issue_keys = {
        key for item in issues.values() for key in item.related_issue_keys()
    }

    missing_issues = related_issue_keys - (issues.keys() | ignore_keys)
    last_missing_issues: set[str] = set()

    while len(missing_issues) > 0 and missing_issues != last_missing_issues:
        last_missing_issues = missing_issues
        log.info(f"Downloading missing issues: {missing_issues}")
        new_issues = download_issues(
            args.jira_server_base,
            jira_token,
            f"key in ({','.join(missing_issues)})",
        )
        for issue in new_issues.values():
            issue.work_logs = []
            issue.work_log_descriptions = []

        issues.update(new_issues)

        related_issue_keys = {
            key for i in issues.values() for key in i.related_issue_keys()
        }
        missing_issues = related_issue_keys - (issues.keys() | ignore_keys)

    if len(missing_issues) > 0:
        log.warning(
            "Could not retrieve the following issues: %s", missing_issues
        )

    prompt = prompt_for(
        issues, missing_issues, args.author_name, args.author_key
    )
    if args.prompt_file is not None:
        print(prompt, file=args.prompt_file)

    result = query_gpt4(
        open_api_token,
        prompt,
        args.dont_send,
        model_str="gpt-4-0125-preview",
        max_tokens=4096,
    )
    if result is None:
        print("No result")
        return 0
    for line in result.splitlines():
        print(f"{line}", file=args.output_file)
    return 0


if __name__ == "__main__":
    sys.exit(main())
