import dataclasses
import re
from datetime import date, datetime
from typing import Optional

from raw_issue_dict import RawJiraIssueDict

# Initialize a set to exclude false positives that match the issue key regex but
# are not Jira issues
to_exclude = {"GPT-4", "GPT-3", "CVE-2022"}


def not_blank(s: str) -> bool:
    """Return True if string contains a non-whitespace element."""
    return re.match(r"\s*$", s) is None


@dataclasses.dataclass
class WorkLog:
    """A work log entry.

    Attributes:
        description: A description of the work log entry.
        hours: The number of hours spent on the work log entry.
        date: The date of the work log entry.
        author_key: The key (id) of the author of the work log entry.
    """

    description: str
    hours: float
    date: date
    author_key: str = "Missing author id"


def parse_iso_format_with_timezone(date_str: str) -> date:
    dt = datetime.fromisoformat(date_str)
    return dt.date()


@dataclasses.dataclass
class Issue:
    key: str
    _issue_summary: str
    work_log_descriptions: list[str]
    components: set[str]
    work_logs: list[WorkLog]

    def __init__(
        self,
        key: str = "Missing key",
        issue_summary: str = "Missing summary",
        worklog_descriptions: Optional[list[str]] = None,
        components: Optional[set[str]] = None,
        work_logs: Optional[list[WorkLog]] = None,
    ):
        if worklog_descriptions is None:
            worklog_descriptions = []
        if components is None:
            components = {}
        if work_logs is None:
            work_logs = []
        self.key = key
        self._issue_summary = self.clean_up_summary(issue_summary)
        self.work_log_descriptions = worklog_descriptions
        self.components = components
        self.work_logs = work_logs

    @property
    def issue_summary(self) -> str:
        return self._issue_summary

    @issue_summary.setter
    def issue_summary(self, value: str) -> None:
        self._issue_summary = self.clean_up_summary(value)

    def for_prompt(self) -> str:
        out: list[str] = list()
        out.append(f"{self.key}:{self.issue_summary}")
        # To deduplicate, you'll need to add up the hours and give a date range
        for log in self.work_logs:
            desc = log.description.strip()
            first_line = True
            for line in desc.splitlines():
                if first_line:
                    first_line = False
                    out.append(f" - {log.date} for {log.hours:.2f}h - {line}")
                    continue
                out.append(f"   {line}")
        return "\n".join(out)

    @staticmethod
    def _keys(s: str) -> set[str]:
        return set(re.findall(r"\b[A-Z]{3,}-\d+\b", s)) - to_exclude

    def related_issue_keys(self) -> set[str]:
        keys = self._keys(self.issue_summary)
        for log in self.work_log_descriptions:
            keys |= self._keys(log)
        keys -= {self.key}
        return keys

    @staticmethod
    def from_jira(jira_dict: RawJiraIssueDict) -> "Issue":
        """Create an Issue object from a Jira issue dictionary."""
        raw_work_logs = jira_dict["fields"]["worklog"]["worklogs"]
        work_logs = [
            WorkLog(
                description=log["comment"],
                hours=log["timeSpentSeconds"] / 3600,
                date=parse_iso_format_with_timezone(log["started"]),
                author_key=log["author"]["key"],
            )
            for log in raw_work_logs
        ]
        return Issue(
            key=jira_dict["key"],
            issue_summary=jira_dict["fields"]["summary"],
            worklog_descriptions=[log["comment"] for log in raw_work_logs],
            components={
                component["name"]
                for component in jira_dict["fields"]["components"]
            },
            work_logs=work_logs,
        )

    @staticmethod
    def clean_up_summary(raw_summary: str) -> str:
        """Change ticket summary that uses our FHIR team jargon to be more
        generic and understandable to the OpenAI API."""
        m = re.findall(r"\b(FHIR-\d+) Helper", raw_summary)
        if m:
            return f"Review and help with {m[0]}"
        m = re.findall(r"\bEric GTD (\d+-\d+-\d+)", raw_summary)
        if m:
            return (
                f"Miscellaneous work for the FHIR API during the sprint "
                f"ending on {m[0]}"
            )
        m = re.findall(r"\bIRAD 2h/developer for (\d+-\d+-\d+)", raw_summary)
        if m:
            return (
                "Individually directed projects and professional "
                f"development during the sprint ending on {m[0]}"
            )
        m = re.findall(r"\(Due \d*\s*(\w+).*?\) Eric ERS", raw_summary)
        if m:
            return f"Produce monthly effort reports for the month of {m[0]}"
        m = re.findall(r"\(Due \d*\s*(\w+).*?\) Eric MSR", raw_summary)
        if m:
            return f"Produce monthly status reports for the month of {m[0]}"
        m = re.findall(r"\bCatch-all.*?to (\d+-\d+-\d+)", raw_summary)
        if m:
            return (
                "Miscellaneous work including meetings, email, "
                "Slack messaging, time tracking, etc. during the sprint "
                f"ending on {m[0]}"
            )
        m = re.findall(
            r"\bSprint start/end for sprint ending (\d+-\d+-\d+)", raw_summary
        )
        if m:
            return (
                f"Record keeping and planning for the sprint ending on {m[0]}"
            )
        m = re.findall(
            r"\bPrep for FHIR sprint review (\d+-\d+-\d+)", raw_summary
        )
        if m:
            return (
                "Review ticket acceptance criteria and create the sprint "
                "accomplishment statement and demos to show at the Sprint "
                f"Review meeting on {m[0]}"
            )
        m = re.findall(r"(.*)\b\(Due .*?\)\s*(.*)", raw_summary)
        if m:
            prefix, suffix = m[0]
            return f"{prefix} {suffix}" if not_blank(prefix) else suffix
        return raw_summary
