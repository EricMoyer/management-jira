import math
from dataclasses import dataclass
from functools import cached_property
from pathlib import Path

import numpy as np
import matplotlib.pyplot as plt
from numpy.random import Generator

from lognormal import LogNormalAlternate

# Capacity distribution parameters
capacity_mean = 60
capacity_std = 15
sick_day_prob = 0.1
sick_day_hours = 8

# Task distribution parameters
task_means = [25, 25, 30, 40]
task_stds = [13, 13, 18, 32]

# Colors for the diagrams (Taken from Wong color-blind friendly palette)
dev_colors = ["#56B4E9", "#0072B2"]
task_colors = ["#009E73", "#F0E442", "#D55E00", "#CC79A7"]


def sample_capacity(rng: Generator) -> np.ndarray:
    """Return a sample from the capacity distribution

    An array of two values is returned, one for each developer.
    """
    capacity = rng.normal(capacity_mean, capacity_std, 2)
    sick_days = rng.binomial(1, sick_day_prob, 2)
    capacity -= sick_days * sick_day_hours
    return capacity


def sample_tasks(rng: Generator) -> list[float]:
    """Return one sample from the task distribution

    return_value[i] is the sample from the task distribution for task i"""
    dists = [
        LogNormalAlternate(mean, std)
        for mean, std in zip(task_means, task_stds)
    ]
    return [dist.sample(rng) for dist in dists]


def estimates_box_diagram_svg(
    pixels_per_hour: float, devs: list[float], tasks: list[float]
) -> str:
    """Generate an SVG string for a box diagram comparing developer
    capacity to task estimates

    This diagram includes labels for each developer and task, and colors
    the boxes according to the colors in dev_colors and task_colors

    :param pixels_per_hour: The number of pixels per hour to use for each
        individual box
    :param devs: The mean capacity of each developer in hours ignoring sick days
    :param tasks: The median estimate of each task in hours

    :return: The SVG string for the box diagram
    """
    # Calculate the maximum width based on the largest of devs and tasks
    max_box_width = max(sum(devs), sum(tasks))

    # Calculate the total width and height of the SVG
    total_width = (
        max_box_width * pixels_per_hour + 80
    )  # Add extra width for labels
    margin = 20
    bar_height = 30
    text_height = bar_height * 0.75

    dev_bar_top = margin
    dev_bar_bot = dev_bar_top + bar_height
    dev_bar_text = dev_bar_bot - 8

    inner_margin = bar_height

    t_bar_top = dev_bar_bot + inner_margin
    t_bar_bot = t_bar_top + bar_height
    t_bar_text = t_bar_bot - 8

    total_height = t_bar_bot + margin

    # Generate the SVG header
    svg = (
        f'<svg xmlns="http://www.w3.org/2000/svg" width="{total_width}" '
        f'height="{total_height}">\n'
    )

    # Generate the capacity boxes
    svg += (
        f'<text x="0" y="{dev_bar_text}" '
        f'font_size="{text_height}px"'
        f">Capacity</text>\n"
    )
    x = 80  # Start the boxes after the label
    for i, dev in enumerate(devs):
        width = dev * pixels_per_hour
        svg += (
            f'<rect x="{x}" y="{dev_bar_top}" width="{width}" height='
            f'"{bar_height}" fill="{dev_colors[i]}"/>\n'
        )
        svg += (
            f'<text x="{x + 5}" y="{dev_bar_text}" '
            f'font_size="{text_height}px">Dev {i + 1}</text>\n'
        )
        x += width

    # Generate the task boxes
    svg += (
        f'<text x="0" y="{t_bar_text}" font_size="{text_height}px"'
        f">Tasks</text>\n"
    )
    x = 80  # Start the boxes after the label
    for i, task in enumerate(tasks):
        width = task * pixels_per_hour
        svg += (
            f'<rect x="{x}" y="{t_bar_top}" '
            f'width="{width}" height="{bar_height}" '
            f'fill="{task_colors[i]}"/>\n'
        )
        svg += f'<text x="{x + 5}" y="{t_bar_text}">T{i + 1}</text>\n'
        x += width

    # Close the SVG tag
    svg += "</svg>"

    return svg


def plot_estimates():
    with open("estimates.svg", "w") as f:
        f.write(
            estimates_box_diagram_svg(
                5, [capacity_mean, capacity_mean], task_means
            )
        )


@dataclass(frozen=True)
class PossibleFuture:
    capacities: list[float]
    tasks: list[float]

    @staticmethod
    def sample(
        generator: Generator, num_selected_tasks: int
    ) -> "PossibleFuture":
        assert num_selected_tasks <= len(task_means), "Too many tasks selected"
        assert num_selected_tasks >= 0, "Must select at least 0 tasks"
        capacities = sample_capacity(generator).tolist()
        tasks = [float(t) for t in sample_tasks(generator)]
        return PossibleFuture(capacities, tasks[:num_selected_tasks])

    @staticmethod
    def samples(
        num_samples: int,
        rng: Generator,
        num_selected_tasks: int = len(task_means),
    ) -> list["PossibleFuture"]:
        return [
            PossibleFuture.sample(rng, num_selected_tasks)
            for _ in range(num_samples)
        ]

    @cached_property
    def num_completed_tasks(self) -> int:
        total_cost_of_tasks = 0
        for i in range(len(self.tasks)):
            total_cost_of_tasks += self.tasks[i]
            if total_cost_of_tasks > self.total_capacity_hours:
                return i
        return len(self.tasks)

    @cached_property
    def total_task_hours(self) -> float:
        return sum(self.tasks)

    @cached_property
    def total_capacity_hours(self) -> float:
        return sum(self.capacities)

    @cached_property
    def backlog_work(self) -> float:
        hours = self.total_capacity_hours - self.total_task_hours
        return max(0.0, hours)

    def select_tasks(self, num_selected_tasks: int) -> "PossibleFuture":
        return PossibleFuture(self.capacities, self.tasks[:num_selected_tasks])


@dataclass
class PossibleFutureBox:
    """A possible future and its position in an SVG diagram"""

    left: float
    top: float
    pf: PossibleFuture


@dataclass(frozen=True)
class PossibleFutureDiagramParameters:
    """Parameters for the possible future diagram

    Note that some properties return a mutable object (dict or list). However,
    mutating these or their contents will cause problems - don't."""

    horizontal_inner_margin: float
    vertical_inner_margin: float
    bar_height: float
    pixels_per_hour: float
    futures_across: int
    num_tasks: int
    samples: list[PossibleFuture]

    @cached_property
    def section_text_height(self) -> float:
        """The height of the section title text"""
        return 5 * self.bar_height

    @cached_property
    def section_text_vertical_margin(self) -> float:
        """The vertical space between the section title and the first row
        of possible futures"""
        return self.bar_height

    @cached_property
    def sample_max_hours(self) -> list[float]:
        """The maximum hours in each sample (capacity or task)"""
        return [
            max(s.total_capacity_hours, s.total_task_hours)
            for s in self.samples
        ]

    @cached_property
    def max_hours(self) -> float:
        """The maximum hours over all samples"""
        return max(t for t in self.sample_max_hours)

    @cached_property
    def svg_width(self) -> float:
        """The width of the entire SVG diagram"""
        return (
            self.futures_across * self.cell_width
            + (self.futures_across - 1) * self.horizontal_inner_margin
        )

    @cached_property
    def vertical_bar_separation(self) -> float:
        """The vertical space between the two rows of bars in a cell"""
        return self.bar_height * 0.5

    @cached_property
    def cell_width(self) -> float:
        """The width of each cell in the diagram"""
        return self.max_hours * self.pixels_per_hour

    @cached_property
    def cell_height(self) -> float:
        """The height of each cell in the diagram"""
        return 2 * self.bar_height + self.vertical_bar_separation

    @cached_property
    def futures_by_completion(self) -> dict[int, list[PossibleFuture]]:
        """The possible futures grouped by the number of completed tasks"""
        return {
            # This is O(num_tasks * num_samples) but num_tasks is small
            i: [pf for pf in self.samples if pf.num_completed_tasks == i]
            for i in range(self.num_sections)
        }

    @cached_property
    def num_sections(self) -> int:
        return self.num_tasks + 1

    @cached_property
    def section_height(self) -> dict[int, float]:
        """The height of each section in the diagram"""
        return {
            i: self.section_text_height
            + self.section_text_vertical_margin
            + (math.ceil(len(futures) / self.futures_across)) * self.cell_height
            for i, futures in self.futures_by_completion.items()
        }

    @cached_property
    def svg_height(self) -> float:
        """The height of the entire SVG diagram"""
        return sum(self.section_height.values())

    @cached_property
    def svg_open_element(self) -> str:
        """The opening SVG element"""
        return (
            f'<svg xmlns="http://www.w3.org/2000/svg" width="{self.svg_width}" '
            f'height="{self.svg_height}">\n'
        )

    @cached_property
    def svg_close_element(self) -> str:
        """The closing SVG element"""
        return "</svg>"

    @cached_property
    def section_tops(self) -> dict[int, float]:
        """The y-coordinate of the top of each section"""
        t: dict[int, float] = {0: 0}  # This could also be a list
        for i in range(1, self.num_sections):
            t[i] = t[i - 1] + self.section_height[i - 1]
        return t

    def section_elements(self, section: int) -> str:
        """Return the SVG elements for a single section"""
        return (
            self.section_text_element(section)
            + "\n"
            + self.futures_elements(section)
        )

    @property
    def section_elements_for_all_sections(self) -> str:
        """Return the SVG elements for all sections"""
        return "\n".join(
            self.section_elements(i) for i in range(self.num_sections)
        )

    def section_text_element(self, section: int) -> str:
        """Return the SVG elements for the text header of a section

        :param section: The section number
        """
        top = self.section_tops[section]
        return (
            f'<text x="0" y="{top + self.section_text_height}" '
            f'font_size="{self.section_text_height}px">{section} Tasks '
            f"Completed</text>"
        )

    def _bar_row(
        self, sizes: list[float], colors: list[str], top: float, left: float
    ) -> str:
        """Return the SVG elements for a row of bars

        :param sizes: The sizes of the bars in hours
        :param colors: The colors of the bars
        :param top: The y coordinate of the top of the row
        :param left: The x coordinate of the left of the row

        :return: A string containing the  SVG elements for the row of bars
        """
        assert len(colors) >= len(sizes), "Not enough colors for a bar plot"
        widths = [size * self.pixels_per_hour for size in sizes]
        # Would be more efficient as a fold, but this is good enough for small
        # lists
        lefts = [left + sum(widths[:i]) for i in range(len(widths))]
        elements = [
            f'<rect x="{left}" y="{top}" width="{width}" height='
            f'"{self.bar_height}" fill="{color}"/>'
            for (left, width, color) in zip(lefts, widths, colors)
        ]
        return "\n".join(elements)

    def dev_capacity_elements(
        self, capacities: list[float], top: float, left: float
    ) -> str:
        """Return the SVG elements for colored rectangles in the developer
        capacity row"""
        return self._bar_row(capacities, dev_colors, top, left)

    def task_elements(self, tasks: list[float], top: float, left: float) -> str:
        """Return the SVG elements for colored rectangles in the task row"""
        return self._bar_row(tasks, task_colors, top, left)

    def single_future_elements(
        self, future_index: int, pf: PossibleFuture, section_top: float
    ) -> str:
        """Return the SVG elements for a single possible future in a section"""
        row = future_index // self.futures_across
        col = future_index % self.futures_across
        left = col * (self.cell_width + self.horizontal_inner_margin)
        top = section_top + row * self.cell_height

        return self.dev_capacity_elements(
            pf.capacities, top, left
        ) + self.task_elements(
            pf.tasks, top + self.bar_height + self.vertical_bar_separation, left
        )

    def futures_elements(self, section: int) -> str:
        """Return the SVG elements for the possible futures in a section

        :param section: The section number
        """
        top = (
            self.section_tops[section]
            + self.section_text_height
            + self.section_text_vertical_margin
        )
        futures = self.futures_by_completion[section]
        elements = [
            self.single_future_elements(i, pf, top)
            for i, pf in enumerate(futures)
        ]
        return "\n".join(elements)


def possible_futures_svg(
    futures_across: int,
    pixels_per_hour: float,
    samples: list[PossibleFuture],
) -> str:
    """Generate an SVG string for a set of possible futures

    This plots them compactly.

    :param futures_across: The number of possible futures to plot horizontally.
        There will be ``ceil(len(samples) / futures_across)`` rows of futures.
        Must be 1 or more.
    :param pixels_per_hour: The number of pixels per hour to use for each
        individual possible future.
    :param samples: The possible futures to plot. Must be non-empty

    :return: The SVG string for the possible futures diagram
    """
    assert futures_across >= 1, "futures_across must be at least 1"
    assert samples, "samples must be non-empty"
    p = PossibleFutureDiagramParameters(
        horizontal_inner_margin=10,
        vertical_inner_margin=10,
        bar_height=5,
        samples=samples,
        futures_across=futures_across,
        pixels_per_hour=pixels_per_hour,
        num_tasks=len(samples[0].tasks),  # Requires at least one sample
    )

    return (
        p.svg_open_element
        + p.section_elements_for_all_sections
        + p.svg_close_element
    )


def plot_possible_futures(
    futures_across: int,
    pixels_per_hour: float,
    samples: list[PossibleFuture],
    path: Path,
) -> None:
    """
    Plot a set of possible futures, writing them to the file
        possible_futures.svg
    :param futures_across: The number of possible futures to plot horizontally.
        There will be ``ceil(len(samples) / futures_across)`` rows of futures.
        Must be 1 or more.
    :param pixels_per_hour: The number of pixels per hour to use for each
        individual possible future.
    :param samples: The possible futures to plot
    :param path: The path to write the SVG to
    """
    with path.open("w") as f:
        f.write(possible_futures_svg(futures_across, pixels_per_hour, samples))


def plot_completion_rates(samples: list[PossibleFuture]):
    """Plot how often completed at least n tasks"""
    completed_tasks = [s.num_completed_tasks for s in samples]
    max_completed = max(completed_tasks)
    completion_counts = [
        len([c for c in completed_tasks if c >= i])
        for i in range(max_completed + 1)
    ]
    completion_rates = [
        100 * count / len(samples) for count in completion_counts
    ]

    fig, ax = plt.subplots()
    ax.barh(range(max_completed + 1), completion_rates)
    ax.set_yticks(range(max_completed + 1))
    ax.set_yticklabels([f"{i}" for i in range(max_completed + 1)])
    ax.set_xlabel("Completion Rate")
    ax.set_title("Completion Rates")
    plt.tight_layout()
    plt.savefig("completion_rates.svg")

    return completion_rates


def plot_backlog_histogram(samples: list[PossibleFuture]):
    backlog_work = [s.backlog_work for s in samples]
    # At least 10 bins, at most one bin per hour. With fewer points, 20 samples
    # per bin. Have the bins start at -1 so the first bin only contains 0
    # backlog work in the typical condition of one bin per hour.
    one_bin_per_hour = int(max(backlog_work) - (-1)) + 1
    fixed_samples = len(backlog_work) // 20
    num_bins = max(one_bin_per_hour, min(10, fixed_samples))
    bins = np.linspace(-1, max(backlog_work), num=num_bins)

    fig, ax = plt.subplots()
    ax.hist(backlog_work, bins=bins, density=True)
    ax.set_xlabel("Hours of Backlog Work")
    ax.set_ylabel("Probability")
    ax.set_title("Backlog Work Distribution")
    plt.tight_layout()
    plt.savefig("backlog_histogram.svg")


def generate_all_diagrams():
    rng = np.random.default_rng(49)
    plot_estimates()
    small_samples = PossibleFuture.samples(100, rng)
    plot_possible_futures(
        futures_across=5,
        pixels_per_hour=1,
        samples=small_samples,
        path=Path("possible_futures.svg"),
    )
    completion_rates = plot_completion_rates(small_samples)
    num_selected_tasks = max(
        i for i, rate in enumerate(completion_rates) if rate >= 90
    )
    sample_after_selection = [
        pf.select_tasks(num_selected_tasks) for pf in small_samples
    ]
    plot_possible_futures(
        futures_across=5,
        pixels_per_hour=1,
        samples=sample_after_selection,
        path=Path("possible_futures_selected.svg"),
    )
    large_samples = PossibleFuture.samples(200000, rng, num_selected_tasks)
    plot_backlog_histogram(large_samples)


if __name__ == "__main__":
    generate_all_diagrams()
