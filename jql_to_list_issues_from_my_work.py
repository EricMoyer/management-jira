#!/usr/bin/env python3.9

import csv
import sys
import argparse
import re

WORK_DESCRIPTION_COL = 22
ISSUE_SUMMARY_COL = 1
ISSUE_KEYS_COL = 0


def read_csv_and_generate_jql():
    # Define expected CSV headers
    expected_headers = [
        "Issue Key",
        "Issue summary",
        "Hours",
        "Work date",
        "Username",
        "Full name",
        "Period",
        "Account Key",
        "Account Name",
        "Account Lead",
        "Account Category",
        "Account Customer",
        "Activity Name",
        "Component",
        "All Components",
        "Version Name",
        "Issue Type",
        "Issue Status",
        "Project Key",
        "Project Name",
        "Epic",
        "Epic Link",
        "Work Description",
        "Parent Key",
        "Reporter",
        "External Hours",
        "Billed Hours",
        "Issue Original Estimate",
        "Issue Remaining Estimate",
        "Location Name",
    ]

    # Read CSV from standard input
    reader = csv.reader(sys.stdin)
    headers = next(reader)

    # Check if headers match expected headers
    if headers != expected_headers:
        sys.stderr.write("Error: CSV headers do not match expected headers.\n")
        sys.exit(1)

    # Initialize a set to exclude false positives that match the regex but are
    # not Jira issues
    to_exclude = {"GPT-4"}
    # Use a set to avoid duplicate issue keys in the JQL query
    jql_keys = set()

    # Find issue keys in the "Issue keys", "Issue summary" and "Work
    # Description" columns
    for row in reader:
        jql_keys.add(row[ISSUE_KEYS_COL])
        for field in [row[ISSUE_SUMMARY_COL], row[WORK_DESCRIPTION_COL]]:
            matches = re.findall(r"\b[A-Z]{3,}-\d+\b", field)
            for match in matches:
                if match not in to_exclude:
                    jql_keys.add(match)

    # Generate JQL query
    jql_query = f"key in ({','.join(str(key) for key in jql_keys)})"
    print(jql_query)


def main():
    parser = argparse.ArgumentParser(
        description="Generate a JQL query to search Jira for all issue keys "
        'present in the "Issue Summary", "Work Description", or '
        '"Issue Key" column of a CSV read from STDIN. '
        "You should generate the CSV using Tempo's"
        '"My Work" >> "Export Timesheet" >> "Export to CSV" feature.'
    )
    parser.parse_args()

    read_csv_and_generate_jql()


if __name__ == "__main__":
    sys.exit(main())
