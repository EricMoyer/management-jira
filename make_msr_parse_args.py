import argparse
import dataclasses
import re
from date_helpers import (
    JiraDate,
    default_first_included_date,
    jira_date_str,
    jira_one_month_after,
)
from typing import AnyStr, TextIO


@dataclasses.dataclass
class Args:
    open_api_token_file: str
    jira_personal_access_token_file: str
    debug: bool
    dont_send: bool
    ignore_keys: set[str]
    jira_server_base: str
    author_key: str
    author_name: str
    output_file: TextIO
    prompt_file: TextIO | None
    first_included_date: JiraDate
    first_excluded_date: JiraDate


class RegexpValidator:
    """A validator for argparse that checks if the argument matches a pattern.

    This code adapted from:
    https://gist.github.com/gurunars/449edbccd0de1449b71524c89d61e1c5

    :var _pattern: The pattern to match the argument against
    """

    def __init__(self, pattern: AnyStr) -> None:
        self._pattern: re.Pattern[AnyStr] = re.compile(pattern)

    def __call__(self, value: str) -> str:
        if not self._pattern.match(value):
            raise argparse.ArgumentTypeError(
                f"Argument has to match '{self._pattern.pattern}'"
            )
        return value


def parse_args() -> Args:
    parser = argparse.ArgumentParser(
        "Use the Open AI API to generate a markdown file summarizing the work "
        "done this month."
    )
    # noinspection SpellCheckingInspection
    parser.add_argument(
        "author_key",
        help="The Jira author key of the person whose work the program will "
        "summarize. e.g., 'doej' for John Doe.",
    )
    parser.add_argument(
        "author_name",
        help="The human-readable full name of the person whose work the "
        "program will summarize. e.g., 'John Doe'",
    )
    parser.add_argument(
        "open_api_token_file", help="Contains the Open AI API Token"
    )
    parser.add_argument(
        "jira_personal_access_token_file",
        help="Contains the Jira personal access token",
    )
    parser.add_argument(
        "--debug",
        action=argparse.BooleanOptionalAction,
        help="Enable debug printing",
    )
    parser.add_argument(
        "--dont-send",
        action=argparse.BooleanOptionalAction,
        help="Don't send the generated prompts to OpenAI, print them instead"
        "implies --debug",
    )
    parser.add_argument(
        "--ignore-keys",
        nargs="*",
        help="Ignore these related issue keys (space separated)",
    )
    parser.add_argument(
        "--jira-server-base",
        default="https://jira.ncbi.nlm.nih.gov",
        type=RegexpValidator(r"https?://[a-zA-Z0-9-\.]+(?::\d+)?"),
        help="The base URL for the Jira server to query. Do not include the "
        "trailing '/' character.",
    )
    parser.add_argument(
        "--output-file",
        type=argparse.FileType("w", encoding="utf-8"),
        default="-",
        help="The file to write the output to (- for stdout)",
    )
    parser.add_argument(
        "--prompt-file",
        type=argparse.FileType("w", encoding="utf-8"),
        help="The file to write the prompts to (- for stdout)",
    )
    parser.add_argument(
        "--first-included-date",
        help="First date to be included in the search "
        "for Jira work-logs. If the date is the 15th or later, defaults"
        "to the first of this month. Otherwise, defaults to the first of the"
        "previous month.",
        type=jira_date_str,
        default=default_first_included_date(),
    )
    parser.add_argument(
        "--first-excluded-date",
        help="First date to be excluded from the search "
        "for Jira work-logs. Must be after first included date. "
        "Defaults to one month after first-included-date.",
        type=jira_date_str,
        default=None,
    )

    args = parser.parse_args()
    return Args(
        open_api_token_file=args.open_api_token_file,
        jira_personal_access_token_file=args.jira_personal_access_token_file,
        debug=args.debug,
        dont_send=args.dont_send,
        ignore_keys=set(args.ignore_keys) if args.ignore_keys else set(),
        jira_server_base=args.jira_server_base,
        author_key=args.author_key,
        author_name=args.author_name,
        output_file=args.output_file,
        prompt_file=args.prompt_file,
        first_included_date=args.first_included_date,
        first_excluded_date=(
            args.first_excluded_date
            if args.first_excluded_date
            else jira_one_month_after(args.first_included_date)
        ),
    )
