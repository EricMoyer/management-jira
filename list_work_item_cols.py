#!/usr/bin/env python3.9

import csv
import sys
import argparse
from collections import defaultdict

from worklog_issue import Issue

WORK_DESCRIPTION_COL = 22
ISSUE_SUMMARY_COL = 1
ISSUE_KEYS_COL = 0


def convert_csv_to_markdown():
    # Define expected CSV headers
    expected_headers = [
        "Issue Key",
        "Issue summary",
        "Hours",
        "Work date",
        "Username",
        "Full name",
        "Period",
        "Account Key",
        "Account Name",
        "Account Lead",
        "Account Category",
        "Account Customer",
        "Activity Name",
        "Component",
        "All Components",
        "Version Name",
        "Issue Type",
        "Issue Status",
        "Project Key",
        "Project Name",
        "Epic",
        "Epic Link",
        "Work Description",
        "Parent Key",
        "Reporter",
        "External Hours",
        "Billed Hours",
        "Issue Original Estimate",
        "Issue Remaining Estimate",
        "Location Name",
    ]

    # Read CSV from standard input
    reader = csv.reader(sys.stdin)
    headers = next(reader)

    # Check if headers match expected headers
    if headers != expected_headers:
        sys.stderr.write("Error: CSV headers do not match expected headers.\n")
        sys.exit(1)

    # Group the work item descriptions by issue but keep them in order
    descriptions = defaultdict(Issue)
    for row in reader:
        issue = descriptions[row[ISSUE_KEYS_COL]]
        issue.issue_summary = row[ISSUE_SUMMARY_COL]
        issue.work_log_descriptions.append(row[WORK_DESCRIPTION_COL])

    # Print the work items grouped under the corresponding key
    for issue in descriptions.values():
        print(issue.for_prompt())

    return 0


def main():
    parser = argparse.ArgumentParser(
        description="Generate a markdown file describing the work on each "
        "issue mentioned in a CSV read from STDIN. "
        "Each row is a work log. You should generate the CSV using Tempo's"
        '"My Work" >> "Export Timesheet" >> "Export to CSV" feature.'
    )
    parser.parse_args()

    convert_csv_to_markdown()


if __name__ == "__main__":
    sys.exit(main())
