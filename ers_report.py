#!/usr/bin/env python3.9
import dataclasses

import jira.resources
from beautifultable import BeautifulTable
from collections import defaultdict

import date_helpers
from date_helpers import (
    default_first_included_date,
    jira_one_month_after,
    JiraDate,
)
from jira.resources import Issue, User, Worklog
from time import sleep
from tqdm import tqdm
from typing import (
    Callable,
    Iterable,
    NewType,
    Any,
    Collection,
    Optional,
    TypeVar,
    cast,
)
import argparse
import datetime
import getpass
import lib
import shelve
import sys

Category = NewType("Category", str)

AuthorKey = NewType("AuthorKey", str)

IssueKey = NewType("IssueKey", str)

JiraServer = Any


# noinspection PyProtectedMember,SpellCheckingInspection
class IssueFieldsSub(jira.resources.Issue._IssueFields):
    """This class is never instantiated. It exists just for the type checker
    to not complain about the dynamic fields in _IssueFields

    In reality, all instances of this class are instances of _IssueFields
    """

    subtasks: list["IssueSub"]
    components: list["IssueSub"]
    # customfield_12202 is the field for epic link in NCBI JIRA
    customfield_12202: str


class IssueSub(Issue):
    """This class is never instantiated. It exists just for the type checker
    to not complain about the dynamic fields in _IssueFields

    In reality, all instances of this class are instances of Issue
    """

    fields: IssueFieldsSub


@dataclasses.dataclass
class ComponentIdAndName:
    id: str
    name: str


@dataclasses.dataclass
class AuthorKeyAndName:
    key: AuthorKey
    display_name: str


@dataclasses.dataclass
class WorkRecord:
    author: AuthorKeyAndName
    components: list[ComponentIdAndName]
    epics: list[IssueKey]
    seconds: int
    ticket_id: str

    @property
    def categories(self) -> set[Category]:
        return categories_of(self.components, self.epics)


def team_issues_non_vacation(
    server: JiraServer,
    first_included_date: JiraDate,
    first_excluded_date: JiraDate,
) -> list[IssueSub]:
    """
    Retrieve all issues with work-logs for this month from the server

    server - the Jira object representing the server
    first_included_date - the first date to include in the range of work-logs
    first_excluded_date - the first date to exclude in the range of work-logs
                          must be after first_included_date
    """
    query = (
        "(project in (FHIR, RSNP, HVARD, HVARC, DG, MGV, CTGT, CTGDEV) "
        "AND component NOT IN (Vacation)) "
        f"AND worklogDate >= {first_included_date} "
        f"AND worklogDate < {first_excluded_date}"
    )
    print(f"Searching: <<<{query}>>>", file=sys.stderr)
    return server.search_issues(query, maxResults=10000)


def empty_list_if_missing(
    to_call: Callable[[], list[IssueSub]]
) -> list[IssueSub]:
    """
    Return the result of to_call unless it raises an AttributeError
    in which case it returns an empty list
    """
    try:
        return to_call()
    except AttributeError:
        return []


def subtasks_of(issue: IssueSub) -> list[IssueSub]:
    """
    Return subtasks of issue
    If issue is missing the subtasks field, returns an empty list
    """
    return empty_list_if_missing(lambda: issue.fields.subtasks)


def components_of(issue: IssueSub) -> list[ComponentIdAndName]:
    """
    Return components of issue
    If issue is missing the components field, returns an empty list
    """
    return [
        ComponentIdAndName(str(component.id), component.name)
        for component in empty_list_if_missing(lambda: issue.fields.components)
    ]


# noinspection SpellCheckingInspection
def epics_of(issue: IssueSub) -> list[IssueKey]:
    """
    Return epics attached issue or an empty list
    """
    # customfield_12202 is the field for epic link in NCBI JIRA
    link: str = issue.fields.customfield_12202
    return [] if link is None else [IssueKey(link)]


def author_key(author: User) -> AuthorKey:
    """
    Return the key for the given author or "no_author_key" if missing
    """
    try:
        return author.key
    except AttributeError:
        return AuthorKey("no_author_key")


# Set of strings to be printed at the end for any unknown components to
# enable updating the list
seen_unknown_components: set[str] = set()

FHIR = Category("FHIR")
CLIN_TRIALS = Category("ClinicalTrials.gov")
FHIR_ODSS_22 = Category("ODSS FHIR API FY2022")
FHIR_ODSS_23 = Category("dbGaP Public Metadata API FY2023")
FHIR_NLM_22 = Category("NLM FHIR API FY2022")
FHIR_NLM_23 = Category("dbGaP NLM Funded FHIR FY2023")
FHIR_MGV_22 = Category("MGV funded FHIR API FY2022")
FHIR_ODSS_21 = Category("FHIR API ODSS FY2021")
FHIR_LHC_21 = Category("FHIR API LHC Forms FY2021")
CLOUD_BLAST = Category("CloudBLAST")
OVERHEAD = Category("Overhead")
# Treat maintenance as overhead for now - we should track them separately, but
# this is good enough for now.
MNT_IMP = OVERHEAD  # Category("Maintenance and Improvements")
VACATION = Category("Vacation")
HV_DEV = Category("HuVar Development")
HV_CUR = Category("HuVar Curation")
OTHER = Category("Other")
NA = Category("N / A")


def category(component: ComponentIdAndName) -> Category:
    """
    Return a category for a given component

    "Other" will be used for unknown components and a string will be
    added to the seen_unknown_components set

    "None" will be used by the categories_of method and should not be used here
    """
    # This map uses the component id instead of the name in case there
    # are identically named components in different projects
    # fmt:off
    # noinspection SpellCheckingInspection
    category_map = {
        '30197': OVERHEAD,  # Meetings and Overhead HVARD
        '25508': OVERHEAD,  # Meetings and Overhead RSNP
        '30203': HV_DEV,  # dbSNP Web HVARD
        '30291': HV_DEV,  # Variation Library HVARD
        '30200': HV_DEV,  # Variation Services HVARD
        '30296': HV_CUR,  # ALFA HVARC
        '30201': HV_DEV,  # Variation Viewer HVARD
        '30210': HV_DEV,  # ALFA Build Pipeline HVARD
        '30204': HV_DEV,  # dbVar Web HVARD
        '30208': HV_DEV,  # dbSNP Build Pipeline HVARD
        '30304': HV_CUR,  # dbVar HVARC
        '30295': HV_CUR,  # Submission HVARC
        '25804': HV_DEV,  # ClinVar Related RSNP
        '26707': HV_DEV,  # PopFreq RSNP
        '19700': HV_DEV,  # RefSnp Web Report Product RSNP
        '21707': HV_DEV,  # Variation Services / SPDI RSNP
        '27307': HV_DEV,  # DbSNP Build RSNP
        '26217': HV_DEV,  # Variation Viewer RSNP
        '29613': HV_DEV,  # Variation Lib RSNP
        '26390': HV_DEV,  # DevOps / Deployment / Systems / ... RSNP
        '30290': 'dbGaP',  # DG: dbGaP Product Team
        '30303': HV_CUR,  # HVARC: dbSNP
        '27897': CLOUD_BLAST,  # Product CB
        '27899': CLOUD_BLAST,  # DataProc/Spark JAR CB
        '26218': 'MGV',  # MGV Program RSNP
        '26219': HV_DEV,  # FTP (for RSNP queue)
        '18993': HV_DEV,  # DbSNP Publicity and Training RSNP
        '27594': 'dbGaP',  # dbGaP (for RSNP queue)
        '26220': HV_DEV,  # Sequence viewer / Genome Workbench / ... RSNP
        '28792': 'GA4GH',  # GA4GH Collab RSNP
        '30202': HV_DEV,  # RefSNP Web Page HVARD
        '30206': HV_DEV,  # FTP HVARD
        '30211': HV_DEV,  # dbSNP Submission Pipeline HVARD
        '30199': FHIR_NLM_23,    # dbGaP FHIR HVARD
        '30209': HV_DEV,  # dbVar Build Pipeline HVARD
        '28894': FHIR_NLM_23,    # dbGaP on FHIR DG
        '30212': HV_DEV,  # dbVar Submission Pipeline HVARD
        '30597': FHIR_NLM_23,    # HAPI FHIR Server
        '30596': OVERHEAD,  # FHIR Meetings and Overhead
        '30308': HV_CUR,  # dbGaP Curator Team
        '30205': HV_DEV,  # 1000 Genomes Browser
        '30213': HV_CUR,  # dbGaP Operations Team
        '31298': HV_CUR,        # HVARC: Curation
        '31297': HV_CUR,        # HVARC: Outreach
        '31294': HV_CUR,        # HVARC: Load
        '31296': HV_CUR,        # HVARC: Analysis
        '31299': HV_CUR,        # HVARC: Remap
        '30598': FHIR_NLM_23,   # FHIR: ETL
        '31005': FHIR_ODSS_23,  # FHIR: User Docs
        '31295': HV_CUR,        # HVARC: Review
        '30297': HV_CUR,        # HVARC: User Support/Engagement
        '26992': HV_CUR,        # HVARC: PopFreq/ALFA ?
        '30305': HV_CUR,        # HVARC: Variation Services
        '30302': HV_CUR,        # HVARC: Pipeline
        '30299': HV_CUR,        # HVARC: Data enhancements/Annotations /Fixes
        '30298': HV_CUR,        # HVARC: Documentation
        '32111': HV_DEV,        # ALFA Web
        '32197': HV_DEV,        # Submission Portal
        '32291': HV_DEV,        # HVARD Tasks
        '30300': HV_CUR,        # HVARC: Web Services
        '29991': CLIN_TRIALS,   # Clinical Trials.gov: ingest
        '30307': HV_CUR,        # Variation Viewer
        '30592': CLIN_TRIALS,   # dbgap operation
        '31004': FHIR_NLM_23,   # Clients
        '28891': CLIN_TRIALS,   # PRS
        '32891': CLIN_TRIALS,   # reports
        '31006': FHIR_ODSS_23,  # Internal Docs
        '34490': HV_CUR,        # training
        '30301': HV_CUR,        # Spec and standards
        '26690': FHIR_NLM_23,   # dbGaP Metrics - These are tickets from
                                #     DGDEV or DG

    }
    # fmt:on
    cat = Category(category_map.get(component.id, OTHER))
    if cat == OTHER:
        seen_unknown_components.add(
            f"'{component.id}': " f"OTHER,        # {component.name}"
        )
    return cat


def epic_category(epic: IssueKey) -> Category:
    # This map uses the component id instead of the name in case there
    # are identically named components in different projects
    # fmt:off
    category_map = {
        'FHIR-33': FHIR_LHC_21,      # LHC Forms Epic for FY2021
        'HVARD-665': HV_DEV,         # Ensure Human Variation full
                                     # compatibility with 64-bit GI
        'SNP-11690': HV_CUR,         # dbSNP Operation (it says HVARC, but I see
                                     # some definite development there)
        'FHIR-82': FHIR_ODSS_21,     # Work funded by ODSS for the dbGaP FHIR
                                     # API in FY2021
        'HVARD-17': HV_DEV,          # dbSNP Build 155
        'HVARD-265': HV_DEV,         # dbSNP Build 156
        'HVARD-161': HV_DEV,         # ALFA Release 2
        'HVARD-236': HV_DEV,         # ALFA Release 3
        'MGV-276': HV_DEV,           # MGV Replace Panasas Data Storage
        'PY-557': HV_DEV,            # Upgrade Airflow to 2.0.1 by June 2, 2021
        'SNP-11962': HV_DEV,         # Redesign Variation Services webpages
        'HVARD-1003': HV_DEV,        # ALFA Code Debt Release 3
        'HVARD-1014': HV_DEV,        # dbSNP Build 156 Code Debt
        'FHIR-185': FHIR_ODSS_21,    # Onboard John Olson and Jeffery Bloom
        'FHIR-121': FHIR_ODSS_21,    # Upgrade all FHIR Python software to
                                     # version 3.9
        'HVARD-900': HV_DEV,         # ALFA Genotype Frequency and
                                     # Hardy-Weinberg Calculations
        'FHIR-204': FHIR_LHC_21,     # RAS and Research Data Finder during FY
                                     # 2021
        'FHIR-290': FHIR_LHC_21,     # Increased simulated individual data
        'HVARD-909': HV_DEV,         # HVARD Python upgrade to at least 3.8
        'FHIR-298': FHIR_ODSS_21,    # API can parse credentials from Auth
                                     # service
        'MGV-250': HV_DEV,           # Create Tableau Dashboard reporting to
                                     # display 4 Product Metrics and 6
                                     # Agile/Sprint Productivity Metrics
                                     # Epic MGV-250
        'HVARD-1233': HV_DEV,        # Integrate GF and HWE into ALFA pipeline
        'FHIR-376': FHIR_ODSS_21,    # Enable access to at least 1M
                                     # controlled-data entries
        'FHIR-415': FHIR_ODSS_23,    # Finish dbGaP-FHIR variable mapping app
        'PY-559': HV_DEV,            # Retire python 3.7
        'FHIR-369': FHIR_MGV_22,     # Enable logging to support FHIR
                                     # dashboards for success metrics
        'HVARD-1283': OVERHEAD,      # Human Variation developers attend online
                                     # cloud training courses
        'HVARD-1379': HV_DEV,        # Report MANE selected mRNA in dbSNP
                                     # products
        'FHIR-60': OTHER,            # Backlog items in the dbGaP FHIR API that
                                     # we haven't committed to. Anything that
                                     # gets worked on in this epic, needs to be
                                     # put in another epic.
        'RSNP-4075': HV_DEV,         # Tasks required immediately after
                                     # transition to dbSNP 2.0 - this is an
                                     # ancient epic that people are grabbing
                                     # code debt from
        'SNP-12547': HV_DEV,         # RefSNP Display Changes - Phase II
        'FHIR-642': FHIR_ODSS_21,    # Upload controlled data to FHIR server
        'HVARD-1386': HV_DEV,        # RefSNP Page Improvements Q1 FY22
        'FHIR-660': FHIR_ODSS_22,    # Misc FHIR Tasks for FY22
        'FHIR-702': FHIR_NLM_22,     # Nitro
        'FHIR-670': FHIR_NLM_22,     # Auth access cleanup
        'HVARD-1735': HV_DEV,        # Knowledge Transfer
        'FHIR-757': FHIR_ODSS_22,    # Use only compatible permissions in
        # queries
        'FHIR-760': FHIR_NLM_22,     # Help LHC during FY22 -- importantly this
                                     # is an epic of the RAS integration
                                     # initiative with NLM funding
        'FHIR-688': MNT_IMP,         # Things the dbGaP FHIR API could do to
                                     # increase security
        'GBW-1482': HV_DEV,          # MASTER for sunsetting of 1000G browser
        'FHIR-783': FHIR_ODSS_22,    # FY22 Internal Documentation
        'FHIR-824': FHIR_NLM_22,     # 1 Billion Observations Avail
        'FHIR-787': FHIR_ODSS_22,    # Group coordination
        'FHIR-825': FHIR_NLM_22,     # All typical phenotype data avail
        'HVARC-405': FHIR_ODSS_22,   # Lon dbGaP FHIR work and planning
        'CTGDEV-454': CLIN_TRIALS,   # Milestone: Review, discover and document
                                     # current processes
        'CTGT-19': CLIN_TRIALS,      # Knowledge Transfer - Sys Arch, Design,
                                     # Business Logic
        'HVARD-1833': HV_DEV,        # Security fixes
        'CTGT-13': CLIN_TRIALS,      # Take Over Operational Support
        'HVARD-1834': HV_DEV,        # ClinVar to dbVar dataflow
        'CTGDEV-621': CLIN_TRIALS,   # Execute Alexander Kostyukovsky's tests
                                     # in the command line
        'PY-575': HV_DEV,            # Django upgrade to 3.2
        'HVARD-1395': HV_DEV,        # Update to Django 3.2 LTS by April 2022
        'CTGDEV-683': CLIN_TRIALS,   # Provision clinDev21/22
        'PY-713': HV_DEV,            # Upgrade Airflow to 2.2.4
        'CTGDEV-618': CLIN_TRIALS,   # Epic CTGDEV-618
        'CTGDEV-786': CLIN_TRIALS,   # Epic CTGDEV-786
        'DGDEV-2475': FHIR_NLM_22,   # Luning tickets for the year
        'CTGDEV-787': CLIN_TRIALS,   # Epic CTGDEV-787
        'CTGT-11': CLIN_TRIALS,      # Epic CTGT-11
        'FHIR-785': FHIR_NLM_22,     # Enable AppLog metadata in FHIR logs
                                     # NLM funded because we needed some of this
                                     # for access audit logs
        'CTGDEV-823': CLIN_TRIALS,   # Epic CTGDEV-823
        'FHIR-1140': FHIR_ODSS_22,   # Nitro sustainability
        'FHIR-1334': FHIR_ODSS_22,   # Add RDF params and mods to Observation
        'HVARD-1228': HV_DEV,        # Update HuVar software to Py 3.9+
        'FHIR-929': FHIR_ODSS_23,    # FY23 Public documentation
        'FHIR-1369': FHIR_ODSS_22,   # Search Parameters FY22
        'FHIR-1330': FHIR_ODSS_22,   # Make Nitro handle batch requests so RDF
        'FHIR-1362': FHIR_ODSS_22,   # Make obs avail without _experimental flag
        'FHIR-1368': FHIR_NLM_23,    # Search Params FY23
        'PY-725': HV_DEV,            # Upgrade Airflow to 2.3.4
        'FHIR-1487': FHIR_NLM_23,    # Provide MVP usage data to Log Analysis
                                     # group (LA)
        'HVARD-2322': HV_DEV,        # Epic HVARD-2322
        'FHIR-1477': FHIR_NLM_23,    # Add some pagination to FHIR API
        'CTGDEV-616': CLIN_TRIALS,   # Epic CTGDEV-616
        'FHIR-1475': MNT_IMP,        # Upgrade FHIR Python to 3.9+ before
                                     # Jan 2023
        'FHIR-1570': MNT_IMP,        # FHIR Improvements FY23 Q4
        'FHIR-1571': MNT_IMP,        # FHIR Improvements FY23 Q3
        'FHIR-1572': MNT_IMP,        # FHIR Improvements FY23 Q2
        'FHIR-1573': MNT_IMP,        # FHIR Maintenance FY23 Q4
        'FHIR-1574': MNT_IMP,        # FHIR Maintenance FY23 Q3
        'FHIR-1575': MNT_IMP,        # FHIR Maintenance FY23 Q2
        'FHIR-1576': MNT_IMP,        # FHIR Improvements FY23 Q1
        'FHIR-1577': MNT_IMP,        # FHIR Maintenance FY23 Q1
        'HVARD-2284': HV_DEV,        # Epic HVARD-2284
        'HVARD-2253': HV_DEV,        # Epic HVARD-2253
        'HVARD-2317': HV_DEV,        # HVARD Maintenance FY23 Q1
        'FHIR-1481': FHIR_ODSS_23,   # Misc tasks for FHIR Fiscal Year 2023
                                     # funded by ODSS
        'FHIR-1479': FHIR_NLM_23,    # Initial requests from RDF to help them
                                     # w.r.t. the changes from the end of FY22
        'FHIR-1715': FHIR_ODSS_23,   # Onboard William Kahley
        'DGDEV-3214': FHIR_ODSS_23,  # FY23 Extraction - not sure if this is
                                     # correct funding
        'FHIR-1717': FHIR_NLM_23,    # FHIR RDF Obs SP - Implement all Search
                                     # Parameters the Research Data Finder Team
                                     # has requested for the FHIR Observation
                                     # resource
    }
    # fmt:on
    cat = Category(category_map.get(epic, OTHER))
    if cat == OTHER:
        seen_unknown_components.add(
            f"'{epic}': " f"OTHER,        # Epic {epic}"
        )
    return cat


def categories_of(
    components: Iterable[ComponentIdAndName], epics: Iterable[IssueKey]
) -> set[Category]:
    """
    Return the set of categories corresponding to epics or if there are none,
    the set of categories corresponding to components or the singleton set
    containing 'None' if there are no components
    """
    final_cats = {epic_category(e) for e in epics}
    if len(final_cats) == 0:
        final_cats = {category(c) for c in components}
    return {Category("None")} if len(final_cats) == 0 else final_cats


@dataclasses.dataclass
class WorkRecordCacheEntry:
    last_accessed: datetime.date
    records: list[WorkRecord]


class WorkRecordCache:
    """Stores WorkRecord objects for a given issue along with a last-accessed
    time (for the issue) and refreshes if they are too old. For simplicity
    too old currently means = last accessed on a different date"""

    def __init__(self, server: JiraServer, filename: str):
        self.server = server
        self.db = shelve.open(filename)

    def records(self, issue: IssueSub) -> list[WorkRecord]:
        entry: Optional[WorkRecordCacheEntry] = self.db.get(issue.key, None)
        if entry is None or entry.last_accessed != datetime.date.today():
            records = [
                WorkRecord(
                    AuthorKeyAndName(
                        author_key(worklog.author), worklog.author.displayName
                    ),
                    components_of(issue),
                    epics_of(issue),
                    worklog.timeSpentSeconds,
                    issue.key,
                )
                for worklog in server_work_logs(self.server, issue)
            ]
            entry = WorkRecordCacheEntry(datetime.date.today(), records)
            self.db[issue.key] = entry
            self.db.sync()
        return entry.records

    def remove_records_from(self, issue_keys: Iterable[IssueKey]):
        for issue_key in issue_keys:
            self.db.pop(issue_key, None)
        self.db.sync()


def parse_arguments() -> tuple[
    JiraDate,
    JiraDate,
    float,
    JiraServer,
    Collection[str],
    WorkRecordCache,
    bool,
]:
    parser = argparse.ArgumentParser(
        description="Statistics about "
        "average logged time for completed tickets"
    )
    parser.add_argument(
        "--username",
        help="Username to for basic authentication login. "
        "Defaults to shell username.",
        default=getpass.getuser(),
    )
    # noinspection SpellCheckingInspection
    parser.add_argument(
        "--passenvt",
        help="Name of the environment variable for the password."
        "Will prompt if absent. Do not set the variable using the command "
        "line - put it in a script you source so it is not revealed in "
        "history. Defaults to JIRA_PASS. If not set will also prompt.",
        default="JIRA_PASS",
    )
    parser.add_argument(
        "--debug",
        action="store_true",
        help="Print all the records with their categories",
    )
    parser.add_argument(
        "--overhead-threshold",
        help="If Overhead is greater than this fraction of a person's output "
        "then their Overhead is allocated according to team effort "
        "distribution (since their own non-Overhead work may not be "
        "representative of the beneficiaries of their work since they "
        "spent so much time coordinating) otherwise their Overhead is "
        "allocated to other categories according to their own effort "
        "distribution.",
        type=lib.probability_float,
        default=0.6,
    )
    parser.add_argument(
        "team_members",
        help="List the team members (use the jira "
        "key you would use to @mention that team member)",
        type=str,
        nargs="+",
    )
    parser.add_argument(
        "--first-included-date",
        help="First date to be included in the search "
        "for Jira work-logs. If the date is the 15th or later, defaults"
        "to the first of this month. Otherwise, defaults to the first of the"
        "previous month.",
        type=date_helpers.jira_date_str,
        default=default_first_included_date(),
    )
    parser.add_argument(
        "--first-excluded-date",
        help="First date to be excluded from the search "
        "for Jira work-logs. Must be after first included date. "
        "Defaults to one month after first-included-date.",
        type=date_helpers.jira_date_str,
        default=None,
    )
    # noinspection SpellCheckingInspection
    parser.add_argument(
        "--work-record-cache",
        help="File used to cache work records for issues. Defaults to "
        "ers_workrecord_cache.shelf for Jira work-logs.",
        type=str,
        default="ers_workrecord_cache.shelf",
    )
    args = parser.parse_args()
    first_included_date = args.first_included_date
    if args.first_excluded_date is None:
        first_excluded_date: str = jira_one_month_after(first_included_date)
    else:
        first_excluded_date = args.first_excluded_date
    if date_helpers.date_from_jira(
        first_included_date
    ) >= date_helpers.date_from_jira(first_excluded_date):
        print(
            f"First included date ({first_included_date}) must be before "
            f"first excluded date ({first_excluded_date})",
            file=sys.stderr,
        )
        exit(-21)
    server = lib.ncbi_jira(args.username, password_varname=args.passenvt)
    team_member_keys = args.team_members
    overhead_threshold = args.overhead_threshold
    debug = args.debug
    return (
        first_excluded_date,
        first_included_date,
        overhead_threshold,
        server,
        team_member_keys,
        WorkRecordCache(server, args.work_record_cache),
        debug,
    )


def download_records(
    first_excluded_date: JiraDate,
    first_included_date: JiraDate,
    server: JiraServer,
    team_member_keys: Collection[str],
    work_record_cache: WorkRecordCache,
):
    # Add all issues and subtasks to the list of issues
    to_add = team_issues_non_vacation(
        server, first_included_date, first_excluded_date
    )
    all_issues: list[IssueSub] = []
    all_issue_ids: set[str] = set()
    while len(to_add) > 0:
        cur = to_add.pop()
        if cur.id not in all_issue_ids:
            all_issues.append(cur)
            all_issue_ids.add(str(cur.id))
            to_add.extend(
                [server_fetch(server, subtask) for subtask in subtasks_of(cur)]
            )
    # Download all work-logs for the issues
    records = [
        record
        for issue in tqdm(all_issues)
        for record in work_record_cache.records(issue)
        if record.author.key in team_member_keys
    ]
    return records


RetryReturnType = TypeVar("RetryReturnType")

ExceptionSubclass = TypeVar("ExceptionSubclass", bound=Exception)


def retry_with_exponential_backoff(
    operation: Callable[[], RetryReturnType],
    exception_type: Any,
    error_message: str,
    max_retries: int = 15,
    sleep_backoff: float = 1.25,
) -> RetryReturnType:
    num_retries = 0
    sleep_time = 0.25
    while True:
        try:
            return operation()
        except exception_type as e:
            if num_retries < max_retries:
                sleep(sleep_time)
                num_retries += 1
                sleep_time *= sleep_backoff
            else:
                raise Exception(error_message) from e


def server_work_logs(server: JiraServer, issue: Issue) -> Iterable[Worklog]:
    return retry_with_exponential_backoff(
        lambda: server.worklogs(issue),
        KeyError,
        f"Could not read work-logs from issue {issue.key}",
    )


def server_fetch(
    server: JiraServer, subtask: IssueSub
) -> IssueSub:  # Type for subtask may be wrong
    return retry_with_exponential_backoff(
        lambda: server.issue(subtask.key),
        NotImplementedError,
        f"Could not fetch subtask {subtask.key}",
    )


def statistics(
    records,
) -> tuple[
    dict[AuthorKey, dict[Category, float]],
    dict[Category, float],
    dict[Category, float],
]:
    author_category_totals: dict[
        AuthorKey, dict[Category, float]
    ] = defaultdict(lambda: defaultdict(float))
    category_totals: dict[Category, float] = defaultdict(float)
    for r in records:
        for c in r.categories:
            t = r.seconds / len(r.categories)  # Divide effort evenly among cats
            author_category_totals[r.author.key][c] += t
            category_totals[c] += t
    team_total_non_overhead: float = sum(
        total
        for cat, total in category_totals.items()
        if cat not in (OVERHEAD, VACATION)
    )
    team_wide_fractions: dict[Category, float] = {
        cat: total / team_total_non_overhead
        for cat, total in category_totals.items()
        if cat not in (OVERHEAD, VACATION)
    }
    return author_category_totals, category_totals, team_wide_fractions


def initialize_report_table() -> BeautifulTable:
    table = BeautifulTable()
    # noinspection PyUnresolvedReferences
    table.set_style(BeautifulTable.STYLE_BOX)
    table.columns.header = [
        "Author",
        "Overhead %",
        "Category",
        "Raw Hrs",
        "Percent",
    ]
    return table


def add_author_rows(
    author_category_totals: dict[AuthorKey, dict[Category, float]],
    overhead_threshold: float,
    team_wide_fractions: dict[Category, float],
    output_table: BeautifulTable,
) -> None:
    for author_key_ in sorted(author_category_totals.keys()):
        cat_totals = author_category_totals[author_key_]
        author_total = sum(cat_totals.values())
        overhead = cat_totals.get(OVERHEAD, 0)
        if overhead / author_total > overhead_threshold:
            # Overhead distributed according to team
            overhead_tag = "High"
            output_table.rows.append(
                [author_key_, overhead_tag, OVERHEAD, overhead / 3600, NA]
            )
            for cat, team_wide_cat_frac in team_wide_fractions.items():
                assert cat not in (OVERHEAD, VACATION)
                auth_cat_total = cat_totals.get(cat, 0)
                secs = overhead * team_wide_cat_frac + auth_cat_total
                percent = 100 * secs / author_total
                output_table.rows.append(
                    [
                        author_key_,
                        overhead_tag,
                        cat,
                        auth_cat_total / 3600,
                        percent,
                    ]
                )
        else:
            # Overhead distributed according to individual
            overhead_tag = "Low"
            output_table.rows.append(
                [author_key_, overhead_tag, OVERHEAD, overhead / 3600, NA]
            )
            for cat, total in cat_totals.items():
                if cat not in (OVERHEAD, VACATION):
                    secs = cat_totals[cat]
                    percent = 100 * secs / (author_total - overhead)
                    output_table.rows.append(
                        [
                            author_key_,
                            overhead_tag,
                            cat,
                            cat_totals[cat] / 3600,
                            percent,
                        ]
                    )


def report_table(
    author_category_totals,
    category_totals,
    overhead_threshold,
    team_wide_fractions,
) -> BeautifulTable:
    table = initialize_report_table()
    table.rows.append(
        ["Team wide", NA, OVERHEAD, category_totals[OVERHEAD] / 3600, NA]
    )
    for cat, frac in team_wide_fractions.items():
        table.rows.append(
            ["Team wide", NA, cat, category_totals[cat] / 3600, 100 * frac]
        )
    add_author_rows(
        author_category_totals, overhead_threshold, team_wide_fractions, table
    )
    return table


def issues_with_category(
    records: Collection[WorkRecord], category_: Category
) -> set[IssueKey]:
    return {
        cast(IssueKey, r.ticket_id)
        for r in records
        if category_ in r.categories
    }


def print_potential_problems_footer(records: Collection[WorkRecord]) -> None:
    cats = {c for r in records for c in r.categories}
    print(f"Categories: {cats}")

    # Issues with None have no component and no epic
    # Issues with FHIR haven't been split into either ODSS or LHC funding source
    # Issues with Other had a component or epic that hasn't been categorized
    for bad_category in {Category("None"), FHIR}:
        bad_ids = issues_with_category(records, bad_category)
        if len(bad_ids) > 0:
            print(f"Records with '{bad_category}' entries: {bad_ids}")

    if len(seen_unknown_components) > 0:
        ready_for_pasting = "\n".join(seen_unknown_components)
        print(
            f'WARNING: unknown components were assigned to the "Other" '
            f"category:\n{ready_for_pasting}",
            file=sys.stderr,
        )


def print_debug_report(records) -> None:
    for r in records:
        print(r)


def generate_ers_report() -> None:
    (
        first_excluded_date,
        first_included_date,
        overhead_threshold,
        server,
        team_member_keys,
        work_record_cache,
        is_debug_mode,
    ) = parse_arguments()

    records = download_records(
        first_excluded_date,
        first_included_date,
        server,
        team_member_keys,
        work_record_cache,
    )

    # Calculate totals for the author for each category and overall
    author_category_totals, category_totals, team_wide_fractions = statistics(
        records
    )

    # Print percentages
    print(
        report_table(
            author_category_totals,
            category_totals,
            overhead_threshold,
            team_wide_fractions,
        )
    )

    print_potential_problems_footer(records)
    if is_debug_mode:
        print_debug_report(records)


generate_ers_report()
