#!/usr/bin/env python3.9

import argparse
import csv
import getpass

import date_helpers
import lib
import sys

from collections import defaultdict


def real_type(type_string):
    """
    Return my time category ticket type corresponding to the given
    jira ticket type.
    """
    return {
        "Task": "Story",
        "Improvement": "Story",
        "New Feature": "Story",
        "Epic": "Design",
    }.get(type_string, type_string)


def category(type_string):
    return {
        "Story": "New Development",
        "Design": "New Development",
        "Hot Feature": "New Development",
        "Bug": "Problem",
        "Code Debt": "Problem",
        "Operations": "Operations",
    }.get(type_string, "Unknown")


parser = argparse.ArgumentParser(
    description="CSV file of ticket type/category and percent of time used. "
    "For creating an excel tree map"
)
parser.add_argument(
    "project", help="The project jira queue to search for tickets"
)
parser.add_argument(
    "--username",
    help="Username to for basic authentication login. "
    "Defaults to shell username.",
    default=getpass.getuser(),
)
parser.add_argument(
    "start_date",
    help="Ticket must be closed on or after start date "
    "use JIRA date format YYYY-MM-DD",
    type=date_helpers.jira_date_str,
)
parser.add_argument(
    "--max_results",
    help="Maximum results to return ... defaults to all",
    default=False,
    type=lib.positive_int,
)

args = parser.parse_args()

server = lib.ncbi_jira(args.username)

# noinspection SpellCheckingInspection
closed_issues = server.search_issues(
    "status in (Closed, Resolved) AND project = {} AND "
    "(status changed to Closed after {} OR "
    "status changed to Resolved after {}) AND "
    "issuetype in standardIssueTypes()"  # No children
    "ORDER BY key DESC".format(args.project, args.start_date, args.start_date),
    fields="aggregatetimespent,type",
    maxResults=args.max_results,
)

times_by_type = defaultdict(float)
total_time = 0
for i in closed_issues:
    time = lib.aggregated_actual(i)
    type_ = real_type(str(i.fields.issuetype))
    if time is not None and type_ != "Discussion":
        times_by_type[type_] += time
        total_time += time

# Make rows ahead of time to allow sorting them (so the treemap will work)
rows = [
    (category(type_), type_, "{}".format(time / total_time))
    for type_, time in times_by_type.items()
]

out = csv.writer(sys.stdout)
out.writerow(["Category", "Issue Type", "Fraction of total time"])
for row in sorted(rows):
    out.writerow(row)
