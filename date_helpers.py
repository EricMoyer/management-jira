import argparse
import datetime
import re

from dateutil.relativedelta import relativedelta


def first_of_previous_month(date: datetime.date) -> datetime.date:
    return date.replace(day=1) + relativedelta(months=-1)


def first_of_the_month(date: datetime.date) -> datetime.date:
    """Return a new datetime object for the first of the month for the given
    date"""
    return date.replace(day=1)


def today() -> datetime.date:
    """Return a datetime object for today"""
    return datetime.datetime.now().date()


# A date string of the form YYYY-MM-DD
JiraDate = str


def default_first_included_date() -> JiraDate:
    """
    Return a YYYY-MM-DD string for the default value of the first included
    date.

    If the current date is the 15th or after, return the first of this month.
    Otherwise, return the first of last month.
    """
    today_ = today()
    return jira_date_for(
        first_of_the_month(today_)
        if today_.day >= 15
        else first_of_previous_month(today_)
    )


def jira_one_month_after(date_str: JiraDate) -> JiraDate:
    """
    Return the YYYY-MM-DD string for one month after the given one
    """
    return jira_date_for(date_from_jira(date_str) + relativedelta(months=1))


def jira_date_str(value: str) -> JiraDate:
    """
    For use with argparse.add_argument type as a parameter to the type
    argument: allows only date strings matching YYYY-MM-DD
    """
    if not re.match(r"\d\d\d\d-\d\d-\d\d", value):
        raise argparse.ArgumentTypeError(
            "{} is not a date in the format YYYY-MM-DD".format(value)
        )

    try:
        datetime.datetime.strptime(value, "%Y-%m-%d")
    except Exception as e:
        raise argparse.ArgumentTypeError(
            "{} is not a valid date".format(value)
        ) from e

    return value


def jira_date_for(date: datetime.date) -> JiraDate:
    """
    Return the YYYY-MM-DD string corresponding to date
    """
    return date.strftime("%Y-%m-%d")


def date_from_jira(jira_date: JiraDate) -> datetime.date:
    """
    Return the `datetime.date` object corresponding to the jira date
    string (YYYY-MM-DD format)
    """
    return datetime.datetime.strptime(jira_date, "%Y-%m-%d").date()
