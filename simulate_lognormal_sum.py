#!/usr/bin/env python3.9
from statistics import mean, stdev, quantiles
from typing import Iterable, List

# noinspection PyPackageRequirements
from tap import Tap  # provided by typed-argument-parser
from lognormal import LogNormalStandard, LogNormal, LogNormalAlternate


def sample_sum(distributions: Iterable[LogNormal]) -> float:
    """
    Return a sample from the distribution of the sum of the given
    LogNormal distributions
    """
    return sum((s.sample() for s in distributions))


class ArgParser(Tap):
    """Statistics about sum of samples from log-normal distributions"""

    num_samples: int = 20000  # Number of samples to generate
    standard_params: List[float] = []
    """ Distributions parameterized by the 
    mean and standard deviation of the underlying Gaussian"""
    alternate_params: List[float] = []
    """ Distributions parameterized by the 
    mean and standard deviation of the distribution"""
    distributions: List[LogNormal] = []
    """ Internal argument - use the params argument - I don't have time to 
    figure out how to hide this from users"""

    def process_args(self):
        # Shorten names of param lists
        std = self.standard_params
        alt = self.alternate_params

        if len(std) % 2 != 0 or len(alt) % 2 != 0:
            raise ValueError("The parameters must be an even number of floats.")

        if len(std) + len(alt) < 2:
            raise ValueError(
                "There must be at least one distribution in the sum. "
                "Please use either the --standard-params or "
                "--alternate-params argument to list the distributions "
                "of the random variables to sum. --help for more details"
            )

        if any(i <= 0 for i in alt):
            raise ValueError(
                "Means and standard deviations of log-normal "
                "distributions must be greater than 0"
            )

        standard: List[LogNormal] = [
            LogNormalStandard(std[i], std[i + 1]) for i in range(len(std) // 2)
        ]
        alternate: List[LogNormal] = [
            LogNormalAlternate(alt[i], alt[i + 1]) for i in range(len(alt) // 2)
        ]
        self.distributions = standard + alternate


def main() -> int:
    args: ArgParser = ArgParser(underscores_to_dashes=True).parse_args()
    samples = sorted(
        (sample_sum(args.distributions) for _ in range(args.num_samples))
    )
    mu_sum = mean(samples)
    print(f"Mean sum: {mu_sum}")
    print(f"Standard deviation sum: {stdev(samples, mu_sum)}")
    percentiles = [0.0] + quantiles(samples, n=100)
    for p in [99, 95, 90]:
        print(f"{p}th percentile: {percentiles[p]}")

    return 0


if __name__ == "__main__":
    exit(main())
