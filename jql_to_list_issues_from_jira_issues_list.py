#!/usr/bin/env python3.9

import csv
import sys
import argparse
import re

ISSUE_SUMMARY_COL = 1
ISSUE_KEYS_COL = 8


def read_csv_and_generate_jql() -> int:
    # Define expected CSV headers
    expected_headers = [
        "Issue Type",
        "Summary",
        "Assignee",
        "Status",
        "Resolution",
        "Σ Time Spent",
        "Σ Original Estimate",
        "Resolved",
        "Issue key",
        "Issue id",
    ]

    # Read CSV from standard input
    reader = csv.reader(sys.stdin)
    headers = next(reader)

    # Check if headers match expected headers
    if headers != expected_headers:
        sys.stderr.write("Error: CSV headers do not match expected headers.\n")
        return 3

    # Initialize a set to exclude false positives that match the regex but are
    # not Jira issues
    to_exclude = {"GPT-4", "CVE-2022"}

    # Use a set to avoid duplicate issue keys in the JQL query
    jql_keys = set()

    # Use a different set to see if we need to do another query
    issue_keys = set()

    # Use a dictionary to capture the issues and their summaries
    issues = dict()

    # Find issue keys in the "Issue keys", "Issue summary" and "Work
    # Description" columns
    for row in reader:
        issue_key = row[ISSUE_KEYS_COL]
        summary = row[ISSUE_SUMMARY_COL]

        issues[issue_key] = summary
        jql_keys.add(issue_key)
        issue_keys.add(issue_key)

        matches = set(re.findall(r"\b[A-Z]{3,}-\d+\b", summary)) - to_exclude
        jql_keys |= matches

    # Generate JQL query if we need to search again
    if issue_keys != jql_keys:
        jql_query = f"key in ({','.join(str(key) for key in jql_keys)})"
        print(jql_query)
        return 0

    # Otherwise generate the issue CSV for the final message
    print("No new keys needed. Printing issue csv", file=sys.stderr)
    writer = csv.writer(sys.stdout)
    writer.writerow(["Issue Key", "Issue Summary"])
    for key, summary in issues.items():
        writer.writerow([key, summary])
    sys.stdout.flush()
    return 0


def main():
    parser = argparse.ArgumentParser(
        description="Generate a JQL query to search Jira for all issue keys "
        'present in the "Summary" or "Issue Key" columns of a CSV read from '
        "STDIN. If all keys the JQL would search for are already present "
        'in the "Issue Key" column, it prints out a two-column CSV with '
        "the key and the summary for every row in the input. "
        "You should generate the input CSV a JQL query from either "
        "jql_to_list_issues_from_my_work.py or a previous run of this script."
    )
    parser.parse_args()

    return read_csv_and_generate_jql()


if __name__ == "__main__":
    sys.exit(main())
