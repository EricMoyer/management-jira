#!/usr/bin/env python3.11
import argparse
import csv
import logging
import sys
from collections import defaultdict
from datetime import date
from itertools import zip_longest

from tqdm import tqdm

from query_gpt4 import log, query_gpt4
from worklog_issue import Issue, WorkLog

log.addHandler(logging.StreamHandler(sys.stderr))


def issues_in_supplementary_csv(csv_filename: str) -> dict[str, Issue]:
    """Read supplementary CSV returning a dict of issue keys to issues.
    Dies when CSV headers are unexpected."""
    summary_col = 1
    key_col = 8

    # Define expected CSV headers
    expected_headers = [
        "Issue Type",
        "Summary",
        "Assignee",
        "Status",
        "Resolution",
        "Σ Time Spent",
        "Σ Original Estimate",
        "Resolved",
        "Issue key",
        "Issue id",
    ]

    # Read CSV from standard input
    with open(csv_filename, "r") as csv_file:
        reader = csv.reader(csv_file)
        raw_headers: list[str] = next(reader)
        # Ignore unexpected headers after the ones we use - they won't affect
        # the data we read
        headers = raw_headers[: len(expected_headers)]

        # Check if headers match expected headers
        if headers != expected_headers:
            diffs = [
                f"At idx {idx}. Expected: {pair[0]} Got: {pair[1]}"
                for idx, pair in enumerate(
                    zip_longest(expected_headers, headers)
                )
                if pair[0] != pair[1]
            ]
            diff_text = "\n".join(diffs)
            log.error(
                "Error: related issue CSV headers do not match expected "
                f"headers. Diffs:\n{diff_text}"
            )
            sys.exit(3)

        return {
            row[key_col]: Issue(row[key_col], row[summary_col])
            for row in reader
        }


def issues_from_work_records(csv_filename: str) -> list[Issue]:
    work_description_col = 22
    issue_summary_col = 1
    issue_keys_col = 0
    components_col = 14
    hours_col = 2
    date_col = 3

    # Define expected CSV headers
    expected_headers = [
        "Issue Key",
        "Issue summary",
        "Hours",
        "Work date",
        "Username",
        "Full name",
        "Period",
        "Account Key",
        "Account Name",
        "Account Lead",
        "Account Category",
        "Account Customer",
        "Activity Name",
        "Component",
        "All Components",
        "Version Name",
        "Issue Type",
        "Issue Status",
        "Project Key",
        "Project Name",
        "Epic",
        "Epic Link",
        "Work Description",
        "Parent Key",
        "Reporter",
        "External Hours",
        "Billed Hours",
        "Issue Original Estimate",
        "Issue Remaining Estimate",
        "Location Name",
    ]

    # Read CSV from file
    with open(csv_filename, "r") as csv_file:
        reader = csv.reader(csv_file)
        headers = next(reader)

        # Check if headers match expected headers
        if headers != expected_headers:
            sys.stderr.write(
                "Error: CSV headers do not match expected headers.\n"
            )
            sys.exit(1)

        # Group the work item descriptions by issue but keep them in order
        descriptions = defaultdict(Issue)
        for row in reader:
            key = row[issue_keys_col]
            date_str = row[date_col].strip().split(" ")[0]
            if "/" in date_str:
                month, day, year = [int(s) for s in date_str.split("/")]
            else:
                year, month, day = [int(s) for s in date_str.split("-")]
            issue = descriptions[key]
            issue.key = key
            issue.issue_summary = row[issue_summary_col]
            issue.work_log_descriptions.append(row[work_description_col])
            issue.work_logs.append(
                WorkLog(
                    row[work_description_col],
                    float(row[hours_col]),
                    date(year, month, day),
                )
            )
            issue.components = {
                s.strip().lower() for s in row[components_col].split(",")
            }

    return list(descriptions.values())


# Newline constant to allow joining lists of strings with newlines in f-strings
NL = "\n"


def prompt_for(
    issue: Issue, supplementary_issues: dict[str, Issue], ignore_keys: set[str]
) -> str:
    related_keys = issue.related_issue_keys() - ignore_keys

    num_summaries = len(related_keys)
    if num_summaries == 0:
        summary_section = ""
    elif num_summaries == 1:
        first_issue = supplementary_issues[next(iter(related_keys))]
        summary_section = (
            f"\n\nIssue {first_issue.key} has the "
            f"Jira summary:\n{first_issue.issue_summary}"
        )
    else:
        summary_lines = [
            f"{key}:{supplementary_issues[key].issue_summary}"
            for key in related_keys
        ]
        summary_section = (
            f"\n\nHere are issue summaries for issue keys mentioned above\n"
            f"{NL.join(summary_lines)}"
        )

    return (
        "Here is a Jira ticket summary and its associated work logs "
        "\n\n" + issue.for_prompt() + f"{summary_section}\n\n"
        "Concisely summarize the work done on this ticket without any "
        "preamble or mentioning the ticket number. You are writing the "
        f"next lines that come after someone has written\n{issue.key}:"
    )


def main() -> int:
    parser = argparse.ArgumentParser(
        "Use the Open AI API to generate a "
        "markdown file summarizing the work on each "
        "issue mentioned in a work log CSV read from a file. If work logs or "
        "summaries mention issues not present in summary data, writes a "
        "JQL query to retrieve that summary data and exits with an error. "
    )
    parser.add_argument("api_token_file", help="Contains the Open AI API Token")
    parser.add_argument(
        "work_record_csv",
        help="CSV containing work logs. Each row is a work log. You should "
        "generate this file using Tempo's"
        '"My Work" >> "Export Timesheet" >> "Export to CSV" feature.',
    )
    parser.add_argument(
        "--supplementary-issues-csv",
        type=str,
        help="Issue data from exporting the JQL for related issues.",
    )
    parser.add_argument(
        "--debug",
        action=argparse.BooleanOptionalAction,
        help="Enable debug printing",
    )
    parser.add_argument(
        "--dont-send",
        action=argparse.BooleanOptionalAction,
        help="Don't send the generated prompts to OpenAI, print them instead"
        "implies --debug",
    )
    parser.add_argument(
        "--only-first",
        action=argparse.BooleanOptionalAction,
        help="Stop after the first issue. Implies --debug",
    )
    parser.add_argument(
        "--ignore-keys",
        nargs="*",
        help="Ignore these related issue keys (space separated)",
    )

    args = parser.parse_args()

    if args.dont_send:
        args.debug = True

    if args.only_first:
        args.debug = True

    if args.debug:
        log.setLevel(logging.DEBUG)
        log.debug("Debug logging enabled")

    with open(args.api_token_file, "r") as file:
        api_token = file.read().strip()

    items = [
        i
        for i in issues_from_work_records(args.work_record_csv)
        if "vacation" not in i.components
    ]

    supplementary_issues: dict[str, Issue] = (
        {}
        if args.supplementary_issues_csv is None
        else issues_in_supplementary_csv(args.supplementary_issues_csv)
    )

    supplementary_issues_keys = set(supplementary_issues.keys())
    ignore_keys = args.ignore_keys
    ignore_keys = set() if ignore_keys is None else set(ignore_keys)

    related_issue_keys = {
        key
        for item in (items + list(supplementary_issues.values()))
        for key in item.related_issue_keys()
    } - ignore_keys

    if related_issue_keys != supplementary_issues_keys:
        jql_query = (
            f"key in ({','.join(str(key) for key in related_issue_keys)})"
        )
        log.error(
            "Supplementary issues are missing some related issues. "
            f"JQL query to retrieve related issues:\n{jql_query}"
        )
        return 2

    # Don't clog the debug output with the progress bar if we're not sending
    # the prompts to OpenAI
    to_iterate = items if args.dont_send else tqdm(items)

    for item in to_iterate:
        prompt = prompt_for(item, supplementary_issues, ignore_keys)
        result = query_gpt4(api_token, prompt, args.dont_send)
        if result is None:
            continue
        print(f"{item.key}:{item.issue_summary}")
        for line in result.splitlines():
            print(f" {line}")
        if args.only_first:
            break
    return 0


if __name__ == "__main__":
    sys.exit(main())
