# License

This software is in the public domain because it was originally
created during work hours to help me do work for the United States
Government.

It is completely unsupported, mostly untested, and full of bugs. Use
at your own risk and do not expect any response to anything -
including pull requests. The government did not ask me to make it and
is not responsible for this software. I made it because it was easier
to do so than not. No harm should be blamed on me or them.

It is in a public repository to enable easy sharing - that's it. That
you the reader can potentially benefit from this software is a nice
side-effect. You are entirely responsible for the use to which you put
this.

# Instructions

I mentioned above that it is completely unsupported. So I haven't
written instructions. Someday I might write instructions for my future
self.

After installing all packages, remember to run

```
pre-commit install
```

to enable auto-formatting hooks.
