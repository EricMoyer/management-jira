import json
import logging
from math import sqrt
from time import sleep
from typing import Optional

from requests_ratelimiter import LimiterSession

session = LimiterSession(per_minute=60)
log = logging.getLogger(__name__)
sleep_time = 1.0
sleep_increase_factor = 1.25


def query_gpt4(
    api_token: str,
    prompt: str,
    only_print: bool,
    model_str: str = "gpt-4",
    max_tokens: int = 480,
) -> Optional[str]:
    """Query the open AI model with the given prompt.

    :arg api_token: The API token to use for the query
    :arg prompt: The prompt to send to the model
    :arg only_print: If True, don't actually send the request to the API,
       just log it
    :arg model_str: The model to use for the query
    :arg max_tokens: The maximum number of tokens to return in the response

    :returns: The response from the model, or None if only_print is True
    """
    global sleep_time
    headers = {
        "Authorization": f"Bearer {api_token}",
        "Content-Type": "application/json",
    }

    data = {
        "model": model_str,
        "messages": [{"role": "user", "content": prompt}],
        "max_tokens": max_tokens,
    }

    log.debug("Payload: %s", json.dumps(data))

    if only_print:
        return

    while True:
        response = session.post(
            "https://api.openai.com/v1/chat/completions",
            headers=headers,
            data=json.dumps(data),
        )

        response_json = response.json()

        log.debug("Response: %s", json.dumps(response_json))

        if response.status_code == 429:
            log.warning(
                f"Rate limit exceeded. Sleeping for {round(sleep_time,1)} "
                f"seconds. Error was {response_json['error']['message']}"
            )
            sleep(sleep_time)
            sleep_time *= sleep_increase_factor
            continue

        if response.status_code != 200:
            raise Exception(
                f"Status {response.status_code} - Request to {model_str} "
                f"failed with error: {response_json['error']['message']}"
            )

        break

    # Reduce sleep time each time we are successful - but at half the rate we
    # increase it
    if sleep_time > 1:
        sleep_time /= sqrt(sleep_increase_factor)
    # Keep minimum sleep at 1 to avoid spamming the API if it's overloaded
    if sleep_time < 1:
        sleep_time = 1

    return response_json["choices"][0]["message"]["content"].strip()
